<?php
if ( !defined('BASEPATH')) exit('No direct script access allowed');

class Stock extends CI_Controller {

    function __construct()
	{
        parent::__construct();

        //var_dump($_SESSION);exit;
        
        $this->load->model('MainModel');
        $this->load->model('ProductModel');
        $this->load->model('AttributesModel');
        $this->load->model('MovementModel');
        $this->load->model('NotificationModel');
        $this->load->model('PdfModel');

        $content['notifications'] = $this->NotificationModel->notification();

        $this->template = array(
            'header'    => $this->load->view('layout/header', $content, TRUE),
            'content'   => '',
            'sidebar'   => $this->load->view('layout/sidebar', FALSE, TRUE),
            'footer'    => $this->load->view('layout/footer', FALSE, TRUE),
        );

        $this->isAuthentichated = $this->AccountModel->checkLoggedIn();

        $this->logged_in = $this->AccountModel->checkLogIn();

        $this->session->set_userdata('current_url', current_url());

        $this->AccountModel->gotoLockscreen();

        $this->session->set_userdata('click_time', date_create(date('H:i:s')));
        
    }

    function product_list_data(){


        if($this->input->is_ajax_request()){
            // Beállítjuk az oszlopokat a DataTable-nek
            $columns = array(
             '0' => 'product_select_radio',				
             '1' => 'product_name',				
             '2' => 'product_item_number',
             '3' => 'product_amount',
             '4' => 'product_amount_unit',
             '5' => 'product_min_amount',
             '6' => 'product_status', 
             '7' => 'product_time_created', 
             '8' => 'product_last_edit', 
             '9' => 'product_check', 
         );
 
 
         // Alap tömb a lekérések paraméterezéséhez
         $params = array();
 
         // Kereső kifejezés lekérése
         $search = $this->input->post('search');
         
         // Kereső kifejezés beállítása a lekéréshez
         if( isset($search['value']) && $search['value'] != '' ){
             $params['keyword'] = $search['value'];
         }
 
         // Lekérdezési sorrend beállítása
         $order = $this->input->post('order');
 
         // A DataTable-nek oszlop ID kell, nem név, így a fent deklarált oszlopazonosítókat beállítjuk
         if( isset($order[0]['column']) && isset($order[0]['dir']) && $order[0]['column'] > 1 ){
             $params['order'] = $columns[$order[0]['column']];
             $params['sort'] = $order[0]['dir'];				
         }
 
         // Beállítjuk a lista hosszát a lapozáshoz
         if( is_numeric($this->input->post('start')) && is_numeric($this->input->post('length')) ){		
             $params['start'] = $this->input->post('start');
             $params['length'] = $this->input->post('length');
         }

         $params['inactive'] = '0';
         // Lekérjük az összes találatot
         $count = $this->ProductModel->getProductNum($params);
 
         // Lekérjük a paraméterek alapján a Teáorek listáját és összeállítjuk a választ
         $response = array(
             'data' => $this->ProductModel->getProduct($params),
             'recordsTotal' => $count,
             'recordsFiltered' => $count,			 	
             'post' => $_POST,
             'params' => $params,
             'order' => $order,
         );
         //print_r($response);
 
         // Töröljük a felesleges változókat
         unset($columns);
         unset($search);
         unset($order);
         unset($count);
         unset($params);		
 
 
         // Kiiratjuk a választ
         print json_encode($response);
        }
 
    }
 
    public function product_list(){
 
         if($this->isAuthentichated && $this->logged_in){
 
 
             $this->template['content']   = $this->load->view('movement/product_list',FALSE, TRUE);
 
 
             $this->parser->parse('layout/layout', $this->template);
 
         }
 
    }


    public function release($product_id = '') {

        if($this->input->is_ajax_request()){

            
            //adatok 
            $product_item_number = $this->input->post('product_item_number');
            $movement_amount    = $this->input->post('movement_amount');
            $movement_amount_unit    = $this->input->post('product_amount_unit');

            $params = array(
                'where'     => array('product_item_number' => $product_item_number)
            );

            //termék adatok lekérése
            $product = $this->ProductModel->getProduct($params);

            if($product != FALSE){

                // új termék darabszám meghatározása
                $new_amount = $product[0]['product_amount'] -  $movement_amount;

                //megvizsgáljuk, hogy nem akar-e többet kiadni, mint amennyi a raktárkészlet
                if($new_amount < 0){
    
                    $response = array(
                        'type'      => 'error',
                        'title'     => 'Hiba',
                        'content'   => 'A kivételezési érték túl nagy, nincs ekkora raktárkészleted ebből a termékből!',
                        'class'     => '1'
                    );
                }else{

                    //legeneráljuk a kiadási bizonylat számát
                    $movement_id_code = $this->MovementModel->generate_form_id_code(1);

                    //összeállítjuk a tömböt mentésre
                    $movement_data = array(
                        'movement_id_code'              => $movement_id_code,
                        'movement_user_id'              => $_SESSION['user']['user_id'],
                        'movement_product_id'           => $product[0]['product_id'],
                        'movement_old_amount'           => $product[0]['product_amount'],
                        'movement_movement_type'        => 1,
                        'movement_movement_amount'      => $movement_amount,
                        'movement_new_amount'           => $new_amount,
                        'movement_product_amount_unit'  => $movement_amount_unit,
                        'movement_date'                 => date('Y-m-d H:i:s')
                    );

                    //mentés
                    $saveMovement = $this->MovementModel->saveMovement($movement_data);

                    $movement_id_data= array(
                        'movement_id_code'          => $movement_id_code,
                        'movement_time_created'     => $movement_data['movement_date']
                    );

                    //elmentjük a mozgás ID-t
                    $saveMovement = $this->MovementModel->getMovementId($movement_id_data);

                    $product_data = array(
                        'product_amount'    => $new_amount
                    );

                    //módosítjuk a termék darabszámát
                    $saveProduct = $this->ProductModel->editProduct($product[0]['product_id'], $product_data);

                    //Megvizsgáljuk, hogy minden megtörtént-e
                    if($saveMovement && $saveProduct == TRUE){

                        $log_text = 'Sikeres termék kiadás: kiadás száma: '.$movement_id_code;
    
                        $this->CommonModel->log('success', 'stock/release', $log_text);

                        $response = array(
                            'type'      => 'success',
                            'title'     => 'Siker',
                            'content'   => 'Sikeres kiadás. Bizonylat száma: <b>'.$movement_id_code.'</b><br> A "Nyomtatvány készítése" gombra kattintva elkészítheted a bizonylatot',
                            'button'    => '<button type="button" class="btn btn-default" id="createPdf">Nyomtatvány készítése</button><br><input type="hidden" id="movement_id_code" value="'.$movement_id_code.'">',
                       );

                    }else{

                        $response = array(
                            'type'      => 'error',
                            'title'     => 'Hiba',
                            'content'   => 'Hiba a mozgás mentésekor!',
                            'class'     => '0'
                        );

                    }

                }

   
            }else{

                $response = array(
                    'type'      => 'error',
                    'title'     => 'Hiba',
                    'content'   => 'Nincs ilyen cikkszámú termék!',
                    'class'     => '0'
                );

            }         

            print json_encode($response);


        }else{

            $params = array(
                'where'     => array('product_id' => $product_id)
            );

            $product = $this->ProductModel->getProduct($params);

            $content['product'] = $product[0];

            $this->template['content'] = $this->load->view('movement/movement_2', $content, TRUE);

            $this->parser->parse('layout/layout', $this->template);

        }


    }


    public function revenue(){


        if($this->input->is_ajax_request()){

        }else{

            $this->template['content'] = $this->load->view('movement/movement', FALSE, TRUE);

            $this->parser->parse('layout/layout', $this->template);

        }

    }

    public function livesearch(){

        if($this->input->is_ajax_request()){

            $params['keyword'] = $this->input->post('search');

            $products = $this->ProductModel->getProduct($params);

            $str = "'";

            $data = '<ul class="response">';
            foreach($products as $product){

                $data .= '<li value="'.$product['product_id'].'">'.$product['product_name'].'</li>';

            }

            $data .= '</ul>';

            $response = array(
                'data'  => $data,
            );

            print json_encode($response);
            //var_dump($_POST, $product);
        }

    }

    public function selectProduct(){

        if($this->input->is_ajax_request()){

            $params = array(
                'where' => array('product_id' => $this->input->post('product_id'))
            );

            $product = $this->ProductModel->getProduct($params);

            $params_2 = array(
                'where' => array('attribute_id' => $product[0]['product_amount_unit'])
            );

            $attribute = $this->AttributesModel->getAttribute($params_2);

            $response = array(
                'type'                  => 'success',
                'product_name'          => $product[0]['product_name'],
                'product_item_number'   => $product[0]['product_item_number'],
                'product_amount'        => $product[0]['product_amount'],
                'product_amount_unit'   => $attribute[0]['attribute_shortname']
            );

            print json_encode($response);

        }

    }

    public function addProduct(){

        if($this->input->is_ajax_request()){

            $params = array(
                'where' => array('product_item_number' => $this->input->post('product_item_number'))
            );

            $product = $this->ProductModel->getProduct($params);

            $params_2 = array(
                'where' => array('attribute_id' => $product[0]['product_amount_unit'])
            );

            $attribute = $this->AttributesModel->getAttribute($params_2);

            $movement_amount    = $this->input->post('movement_amount');

            if($product != FALSE){

                $new_amount = $product[0]['product_amount'] +  $movement_amount;

                $data = '
                <tr>
                    <td><input name="movement_product_id['.$product[0]['product_id'].']" value="'.$product[0]['product_name'].'" readonly></td>
                    <td>'.$product[0]['product_item_number'].'</td>
                    <td><input name="movement_old_amount['.$product[0]['product_id'].']" value="'.$product[0]['product_amount'].'" readonly></td>
                    <td>'.$attribute[0]['attribute_shortname'].'</td>
                    <td><input name="movement_movement_amount['.$product[0]['product_id'].']" value="'.$movement_amount.'" readonly></td>
                    <td><input name="movement_product_amount_unit['.$product[0]['product_id'].']" value="'.$attribute[0]['attribute_shortname'].'" readonly></td>
                    <td><input name="movement_new_amount['.$product[0]['product_id'].']" value="'.$new_amount.'" readonly></td>
                    <td>'.$attribute[0]['attribute_shortname'].'</td>
                    <td><button id="delete" class="btn btn-danger">Törlés</button></td>
                </tr>
                ';
    
                $response = array(
                    'type'      => 'success',
                    'content'   => $data,
                );
            }

            print json_encode($response);

        }
    }

    public function saveMovement() {

        if($this->input->is_ajax_request()){

            $product_id = $this->input->post('movement_product_id[]');
            $movement_old_amount = $this->input->post('movement_old_amount[]');
            $movement_product_amount_unit = $this->input->post('movement_product_amount_unit[]');
            $movement_movement_amount = $this->input->post('movement_movement_amount[]');
            $movement_new_amount = $this->input->post('movement_new_amount[]');

            if(!empty($product_id)){
                foreach($product_id as $pid_key => $pid_value){
                
                    $product_ids[] = [$pid_key];
                }
    
                foreach($movement_old_amount as $old_a_key => $old_a_value){
    
                    $movement_old_amounts[] = [$old_a_value];
                }
    
                foreach($movement_movement_amount as $m_amount_key => $m_amount_value){
    
                    $movement_movement_amounts[] = [$m_amount_value];
                }
    
                foreach($movement_product_amount_unit as $amount_u_key => $amount_u_value){
    
                    $movement_product_amount_units[] = [$amount_u_value];
                }
    
                foreach($movement_new_amount as $new_a_key => $new_a_value){
    
                    $movement_new_amounts[] = [$new_a_value];
                }
    
               $movement_data = array();
    
               $movement_id_code = $this->MovementModel->generate_form_id_code(0);
    
               $movement_date = date('Y-m-d H:i:s');
    
                for($i=0; $i < count($product_ids); $i++){
    
                    $movement_datas[$i][] = array(
                        'movement_id_code'                  => $movement_id_code,
                        'movement_user_id'                  => $_SESSION['user']['user_id'],
                        'movement_product_id'               => $product_ids[$i][0],
                        'movement_old_amount'               => $movement_old_amounts[$i][0],
                        'movement_movement_type'            => 0,
                        'movement_movement_amount'          => $movement_movement_amounts[$i][0],
                        'movement_new_amount'               => $movement_new_amounts[$i][0],
                        'movement_product_amount_unit'      => $movement_product_amount_units[$i][0],
                        'movement_date'                     => $movement_date,
                    );
                }

                $movement_id_data= array(
                    'movement_id_code'          => $movement_id_code,
                    'movement_time_created'     => $movement_data['movement_date']
                );

                $saveMovement = $this->MovementModel->getMovementId($movement_id_data);
                
                foreach($movement_datas as $movement_data){
    
                    $this->MovementModel->saveMovement($movement_data[0]);
    
                    $product_data = array(
                        'product_amount'    => $movement_data[0]['movement_new_amount']
                    );
    
                    $this->ProductModel->editProduct($movement_data[0]['movement_product_id'], $product_data);
                }
    
                $log_text = 'Sikeres termék bevételezés: Bevét száma: '.$movement_id_code;
    
                $this->CommonModel->log('success', 'stock/saveMovement', $log_text);
    
                $response = array(
                    'type'      => 'success',
                    'title'     => 'Siker',
                    'content'   => 'Sikeres bevételezés. Bizonylat száma: <b>'.$movement_id_code.'</b><br> A "Nyomtatvány készítése" gombra kattintva elkészítheted a bizonylatot',
                    'button'    => '<button type="button" class="btn btn-default" id="createPdf">Nyomtatvány készítése</button><br><input type="hidden" id="movement_id_code" value="'.$movement_id_code.'">',
                );
    

            }else{

                $response = array(
                    'type'              => 'error',
                    'title'             => 'Hiba',
                    'content'           => 'Nem adtál terméket a listához!',
                );

            }
            print json_encode($response);
        }
    }

    public function storno() {

        if($this->input->is_ajax_request()){

            //adatok
            $product_ids = $this->input->post('product_list_select');

            $storno_date = date('Y-m-d H:i:s');

            $storno_id_code = $this->MovementModel->generate_form_id_code('2');

            //körbejárjuk a törlendő termék id-k tömbjét
            foreach($product_ids as $product_id){

                $params = array(
                    'where'     => array(
                        'movement_product_id'       => $product_id,
                        'movement_id_code'          => $this->session->userdata('storno_form_id'),
                        'movement_is_deleted'       => 0
                    )
                );

                $data = array(
                    'movement_is_deleted' => 1,
                );

                //beállítjuk a mozgást törlésre
                $stornoMovement = $this->MovementModel->stornoMovement($params, $data);

                // megvizsgáljuk, hogy sikeres-e
                if($stornoMovement == TRUE){

                    $params = array(
                        'where' => array(
                            'product_id' => $product_id,
                        )
                    );

                    //lekérjük a termék adatokat
                    $product_data = $this->ProductModel->getProducts($params);

                    $params = array(
                        'where'     => array(
                            'movement_product_id'   => $product_id,
                            'movement_id_code'      => $this->session->userdata('storno_form_id')
                        )
                    );

                    // lekérjük a mozgás adatokat
                    $storno_data = $this->MovementModel->getMovement($params);

                    
                    // beállítjuk a mozgásnak megfelelően, az új darabszámot
                    if($this->session->userdata('movement_type') == '0'){

                        $new_amount = $product_data[0]['product_amount'] - $storno_data[0]['movement_movement_amount'];
                    
                    }else{

                        $new_amount = $product_data[0]['product_amount'] + $storno_data[0]['movement_movement_amount'];
                    
                    }

                    

                    $edit_data = array(
                        'product_amount' => $new_amount,
                        'product_last_edit'     => $storno_date
                    );

                    //módosítjuk a megfelelő termékeket
                    $edit_product = $this->ProductModel->editProduct($product_id, $edit_data);
                    
                    // ha sikeres a termék módosítás
                    if($edit_product == TRUE){

                        $storno = array(
                            'storno_id_code'            => $storno_id_code,
                            'storno_movement_id_code'   => $this->session->userdata('storno_form_id'),
                            'storno_movement_id'        => $storno_data[0]['movement_id'],
                            'storno_date'               => $storno_date
                        );

                        //Elmentjük a sztornó bizonylatot
                        $storno_id = $this->MovementModel->saveStorno($storno);

                        if($storno_id != false){

                            $errors[] = 'false;';

                        }else{

                            $log_text = 'Sikertelen sztornó bizonylat létrehozása, movement_code_id: '.$this->session->userdata('storno_form_id').', product_id: '.$product_id.' storno_id_code: '.$storno_id_code ;

                            $this->CommonModel->log('failed', 'stock/storno', $log_text);
        
                        break;
                            
                        }

                        

                    }else{
                        
                        $log_text = 'Sikertelen termék módosítás sztornózás közben, movement_code_id: '.$this->session->userdata('storno_form_id').', product_id: '.$product_id ;

                        $this->CommonModel->log('failed', 'stock/storno', $log_text);
    
                    break;
                    }

                }else{

                    $log_text = 'Sikertelen mozgás törlés, movement_code_id: '.$this->session->userdata('storno_form_id').', product_id: '.$product_id ;

                    $this->CommonModel->log('failed', 'stock/storno', $log_text);

                break;
                }

            }
            
            //megvizsgáljuk a hibatömböt, hogy tartalmaz-e olyan értéket, amit nem kellene *csak false értéket kell tartalmaznia, ha jól ment minden
            if(isset($errors) && is_array($errors) && in_array('true', $errors, false)){

                $response = array(
                    'type'      => 'error',
                    'title'     => 'Hiba',
                    'content'   => 'Hiba a sztornózás közben!'
                );

            }else{

                //töröljük a feleslegessé vált adatokat a sessionből
                $this->session->unset_userdata('storno_form_id');
                $this->session->unset_userdata('movement_type');

                $log_text = 'Sikeres sztornó bizonylat létrehozása, storno_id_code: '.$storno_id_code ;

                $this->CommonModel->log('success', 'stock/storno', $log_text);

                $response = array(
                    'type'      => 'success',
                    'title'     => 'Siker',
                    'content'   => 'Sikeres sztornózás. Bizonylat száma: <b>'.$storno_id_code.'</b><br> A "Nyomtatvány készítése" gombra kattintva elkészítheted a bizonylatot',
                    'button'    => '<button type="button" class="btn btn-default" id="createPdf">Nyomtatvány készítése</button><br><input type="hidden" id="storno_id_code" value="'.$storno_id_code.'">',
                
                );
            }

            print json_encode($response);

        }else{

            $this->template['content'] = $this->load->view('movement/storno', FALSE, TRUE);

            $this->parser->parse('layout/layout', $this->template);

        }
    }

    public function form_id_codeforStorno(){

        if($this->input->is_ajax_request()){

            //var_dump($_POST);

            $params['keyword'] = $this->input->post('search');
            $params['where'] = array('movement_id_type' => $this->session->userdata('movement_type'));
            //$params['top'] = '1';

            $movements = $this->MovementModel->getMovementId($params);

            //var_dump($params['where'], $movements);exit;

            if($movements == false){

                $data = 
                '<ul class="response">
                    <li>Nincs találat</li>
                </ul>';

            }else{

                $data = '<ul class="response">';
                foreach($movements as $movement){

                    $data .= '<li data-value="'.$movement['movement_id_id_code'].'">'.$movement['movement_id_id_code'].'</li>';

                }
            }

            $data .= '</ul>';

           //var_dump($data);exit;

            $response = array(
                'data'  => $data,
            );

            print json_encode($response);
        }
    }

    public function stornoType(){

        if($this->input->is_ajax_request()){

            //var_dump($_POST);

            $this->session->set_userdata('movement_type', $this->input->post('movement_type'));

            $data = '
                <label for="form_id_code">Bevételezési - / kiadási bizonylat száma</label>
                <input type="text" class="form-control" id="form_id_code">
                <div id="response"></div>
            ';

            $response = array(
                'type'      => 'success',
                'content'   => $data
            );

            print json_encode($response);

        }
    }

    public function selectForm(){

        if($this->input->is_ajax_request()){

            $params['keyword'] = $this->input->post('search');
            $params['where'] = array('movement_is_deleted' => 0);
            //$params['top'] = '1';

            $movements = $this->MovementModel->getMovement($params);

            //var_dump($_POST, $params['where'], $movements);exit;

            if(is_array($movements) && !empty($movements)){
                $data = '
            <label>Termék lista</label>
            <select multiple name="product_list_select[]" id="product_list_select" class="form-control">';
            foreach($movements as $movement){

                $params = array(
                    'where'     => array(
                        'product_id'    => $movement['movement_product_id'])
                );

                $product = $this->ProductModel->getProduct($params);


                $data .= '<option value="'.$movement['movement_product_id'].'">'.$product[0]['product_name'].'</option>';


                $response = array(
                    'type'  => 'success',
                    'movement_id_code'  => $movements[0]['movement_id_code'],
                    'data'  => $data,
                );
            }

            $data .= '</select>';

            }else{
                $data = 
                '<div class="callout callout-warning">
                    <h4>Hiba</h4>

                    <p>Nincs sztornózható termék a bevételezési bizonylaton!!</p>
                </div>';

                $response = array(
                    'type'  => 'error',
                    'data'  => $data,
                );
            }

            $this->session->set_userdata('storno_form_id', $movements[0]['movement_id_code']);


            print json_encode($response);

            //var_Dump($data);

            
        }
    }

    public function pdf(){

        if($this->input->is_ajax_request()){

            $params = array(
                'where' => array('movement_id_code'   => $this->input->post('movement_id_code'))
            );

            $movement_datas = $this->MovementModel->getMovement($params);

            foreach($movement_datas as $key => $value){

                $params = array(
                    'where' => array('product_id'   => $value['movement_product_id'])
                );
    
                $product_data = $this->ProductModel->getProducts($params);

                $movement_datas[$key]['product_name'] = $product_data[0]['product_name'];
                $movement_datas[$key]['product_item_number'] = $product_data[0]['product_item_number'];

            }

            $form_data['form_table_content'] = '';
            $form_data['movement_id_code'] = $this->input->post('movement_id_code');

            $form_data['user'] = $_SESSION['user']['user_username'];
            $form_data['time_created'] = date('Y-m-d H:i:s');

            foreach($movement_datas as $movement_data){

                $form_data['form_table_content'] .= '<tr align = "center">
                <td style="border: 1px solid #000000;">'.$movement_data['product_item_number'].'</td>
                <td style="border: 1px solid #000000;">'.$movement_data['product_name'].'</td>
                <td style="border: 1px solid #000000;">'.$movement_data['movement_old_amount'].' '.$movement_data['movement_product_amount_unit'].'</td>
                <td style="border: 1px solid #000000;">'.$movement_data['movement_movement_amount'].' '.$movement_data['movement_product_amount_unit'].'</td>
                <td style="border: 1px solid #000000;">'.$movement_data['movement_new_amount'].' '.$movement_data['movement_product_amount_unit'].'</td>
                <td style="border: 1px solid #000000;">'.$movement_data['movement_date'].'</td>
                </tr>';
            }

            $form = $this->PdfModel->getForm($this->input->post('form_type'));

            foreach($form_data as $key => $value){

                $form[0]['form_content'] = str_replace("[$key]", $value, $form[0]['form_content']);

            }

            $pdf = $this->PdfModel->createPdf($this->input->post('movement_id_code').' bizonylat', $form[0]['form_content'], TRUE, '../pdf');

            $log_text = 'Sikeres bizonylat készítése raktár mozgás után. Bizonylat száma: '.$this->input->post('movement_id_code');

            $this->CommonModel->log('success', 'stock/pdf', $log_text);

            $response = array(
                'type'       => 'success',
                'title'      => 'Siker',
                'content'    => 'Az OK gombra kattintva megnyithatod a bizonylatot',
                'url'        => $pdf
            );

            print json_encode($response);

        }

    }

    public function stornoPdf(){
        if($this->input->is_ajax_request()){

            $params = array(
                'where' => array('storno_id_code'   => $this->input->post('storno_id_code'))
            );

            $storno_datas = $this->MovementModel->getStorno($params);

            foreach($storno_datas as $key => $value){

                $params = array(
                    'where' => array('movement_id' => $value['storno_movement_id'])
                );

                $movement_data = $this->MovementModel->getMovement($params);

                $params = array(
                    'where' => array('product_id'   => $movement_data[0]['movement_product_id'])
                );
    
                $product_data = $this->ProductModel->getProducts($params);


                $params =array(
                    'where' => array('attribute_id'   => $product_data[0]['product_amount_unit'])
                );

                $attribute_datas = $this->AttributesModel->getAttribute($params);

                $storno_datas[$key]['product_item_number'] = $product_data[0]['product_item_number'];
                $storno_datas[$key]['product_name'] = $product_data[0]['product_name'];
                $storno_datas[$key]['deleted_amount'] = $movement_data[0]['movement_movement_amount'];
                $storno_datas[$key]['amount_unit'] = $attribute_datas[0]['attribute_shortname'];

            }

            $form_data['form_table_content'] = '';
            $form_data['storno_id_code'] = $this->input->post('storno_id_code');

            $form_data['user'] = $_SESSION['user']['user_username'];
            $form_data['time_created'] = date('Y-m-d H:i:s');

            foreach($storno_datas as $storno_data){

                $form_data['form_table_content'] .= '<tr align = "center">
                <td style="border: 1px solid #000000;">'.$storno_data['storno_movement_id_code'].'</td>
                <td style="border: 1px solid #000000;">'.$storno_data['product_item_number'].'</td>
                <td style="border: 1px solid #000000;">'.$storno_data['product_name'].'</td>
                <td style="border: 1px solid #000000;">'.$storno_data['deleted_amount'].'</td>
                <td style="border: 1px solid #000000;">'.$storno_data['amount_unit'].'</td>
                <td style="border: 1px solid #000000;">'.$storno_data['storno_date'].'</td>
                </tr>';
            }

            

            $form = $this->PdfModel->getForm($this->input->post('form_type'));

            foreach($form_data as $key => $value){

                $form[0]['form_content'] = str_replace("[$key]", $value, $form[0]['form_content']);

            }

            $pdf = $this->PdfModel->createPdf($this->input->post('storno_id_code').' bizonylat', $form[0]['form_content'], TRUE, '../pdf');

            $log_text = 'Sikeres sztornó bizonylat készítése. Bizonylat száma: '.$this->input->post('storno_id_code');

            $this->CommonModel->log('success', 'stock/pdf', $log_text);

            $response = array(
                'type'       => 'success',
                'title'      => 'Siker',
                'content'    => 'Az OK gombra kattintva megnyithatod a bizonylatot',
                'url'        => $pdf
            );

            print json_encode($response);

        }
    }

    public function test(){

        $a = array(
            'false',
            'false',
            'false',
            'false',
            'false',
        );

        /*if (count(array_unique($a)) === 1 && end($a) === 'false') {

            print 'ok';
        }else{
            print'nem ok';
        }*/

        if (in_array('false', $a, true)) {
            print 'ok';
        }else{
            print'nem ok';
        }
    }
}