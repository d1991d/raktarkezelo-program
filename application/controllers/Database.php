<?php
if ( !defined('BASEPATH')) exit('No direct script access allowed');

class Database extends CI_Controller {

    function __construct()
	{
        parent::__construct();
        
        $this->load->model('DatabaseModel');
        $this->load->model('NotificationModel');

        $content['notifications'] = $this->NotificationModel->notification();

        $this->template = array(
            'header'    => $this->load->view('layout/header', $content, TRUE),
            'content'   => '',
            'sidebar'   => $this->load->view('layout/sidebar', FALSE, TRUE),
            'footer'    => $this->load->view('layout/footer', FALSE, TRUE),
        );

        $this->isAuthentichated = $this->AccountModel->checkLoggedIn();

        $this->logged_in = $this->AccountModel->checkLogIn();

        $this->session->set_userdata('current_url', current_url());

        $this->AccountModel->gotoLockscreen();

        $this->session->set_userdata('click_time', date_create(date('H:i:s')));

    }

    public function index(){

        if($this->isAuthentichated && $this->logged_in){

            $dir = "../databases/";
            $database=scandir($dir, -1);
            $database = array_diff($database, array('.', '..'));

            $content['databases'] = $database;
            $this->template['content']   = $this->load->view('database/list', $content, TRUE);

            $this->parser->parse('layout/layout', $this->template);
        }

    }


    public function saveDB(){

        if($this->input->is_ajax_request()){
            // Load the DB utility class
            $this->load->dbutil();

            $date = date('Y-m-d H-i-s');

            // Backup your entire database and assign it to a variable
            $backup = $this->dbutil->backup();

            // Load the file helper and write the file to your server
            $this->load->helper('file');
            
            $save_db = write_file('../databases/'.$date.'.gz', $backup);

            if($save_db == TRUE){

                $data = array(
                    'db_name'           => $date.'.gz',
                    'db_time_created'   => date('Y-m-d H:i:s')
                );

                $this->DatabaseModel->saveDB($data);

                $log_text = 'Sikeres adatbázis mentés név: '.$date.'.gz';
    
                $this->CommonModel->log('success', 'database/saveDB', $log_text);

                $response = array(
                    'type'      => 'success',
                    'title'     => 'Siker',
                    'content'   => 'Sikeresen létrehoztad az adatbázis mentést!'
                );

            }else{

                $log_text = 'Sikertelen adatbázis mentés';
    
                $this->CommonModel->log('failed', 'database/saveDB', $log_text);

                $response = array(
                    'type'      => 'error',
                    'title'     => 'Hiba',
                    'content'   => 'Hiba az adatbázis mentésekor!'
                );

            }

            print json_encode($response);
            // Load the download helper and send the file to your desktop
            /*$this->load->helper('download');
            force_download('mybackup.gz', $backup);*/
        }
    }
}