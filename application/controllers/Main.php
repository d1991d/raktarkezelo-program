<?php
if ( !defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    function __construct()
	{
        parent::__construct();
        
        $this->load->model('MainModel');
        $this->load->model('ProductModel');
        $this->load->model('NotificationModel');

        $this->template = array(
            'header'    => '',
            'content'   => '',
            'sidebar'   => '',
            'footer'    => '',
        );

        $this->isAuthentichated = $this->AccountModel->checkLoggedIn();

        $this->logged_in = $this->AccountModel->checkLogIn();

        //var_dump($_SESSION);exit;

        $this->session->set_userdata('current_url', current_url());

        $this->AccountModel->gotoLockscreen();

        $this->session->set_userdata('click_time', date_create(date('H:i:s')));

    }

    public function index(){
        
        
        if($this->isAuthentichated && $this->logged_in){


            $params['inactive'] = '0';

            if($this->ProductModel->getProduct($params) == false){
                $content['active_product_num'] = '0';
            }else{
                $content['active_product_num'] = count($this->ProductModel->getProduct($params));
            }

            $params_2['inactive'] = '1';

            if($this->ProductModel->getProduct($params_2) == false){
                $content['inactive_product_num'] = '0';
            }else{
                $content['inactive_product_num'] = count($this->ProductModel->getProduct($params_2));
            }

            $params_3['low_amount'] = '1';

            if($this->ProductModel->getProduct($params_3) == false){
                $content['low_amount_product_num'] = '0';
            }else{
                $content['low_amount_product_num'] = count($this->ProductModel->getProduct($params_3));
            }

            if($this->ProductModel->getProduct() == false){
                $content['sum_product_num'] = '0';
            }else{
                $content['sum_product_num'] = count($this->ProductModel->getProduct());
            }

            //var_dump($this->ProductModel->getProduct($params_3));exit;

            $content['notifications'] = $this->NotificationModel->notification();

            //var_dump($contents);exit;

            $this->template['header']    = $this->load->view('layout/header', $content, TRUE);
            $this->template['content']   = $this->load->view('main',FALSE, TRUE);
            $this->template['sidebar']   = $this->load->view('layout/sidebar', FALSE, TRUE); 
            $this->template['footer']    = $this->load->view('layout/footer', FALSE, TRUE);

            $this->parser->parse('layout/layout', $this->template);
        }else{

            redirect('/bejelentkezes');


        }
    }

    function low_amount_product_list(){


        if($this->input->is_ajax_request()){
            // Beállítjuk az oszlopokat a DataTable-nek
            $columns = array(			
             '0' => 'product_name',				
             '1' => 'product_item_number',
             '2' => 'product_amount',
             '3' => 'product_amount_unit',
             '4' => 'product_min_amount',
             '5' => 'product_status', 
             '6' => 'product_time_created', 
             '7' => 'product_last_edit', 
             '8' => 'product_check', 
         );
 
 
         // Alap tömb a lekérések paraméterezéséhez
         $params = array();
 
         // Kereső kifejezés lekérése
         $search = $this->input->post('search');
         
         // Kereső kifejezés beállítása a lekéréshez
         if( isset($search['value']) && $search['value'] != '' ){
             $params['keyword'] = $search['value'];
         }
 
         // Lekérdezési sorrend beállítása
         $order = $this->input->post('order');
 
         // A DataTable-nek oszlop ID kell, nem név, így a fent deklarált oszlopazonosítókat beállítjuk
         if( isset($order[0]['column']) && isset($order[0]['dir']) && $order[0]['column'] > 1 ){
             $params['order'] = $columns[$order[0]['column']];
             $params['sort'] = $order[0]['dir'];				
         }
 
         // Beállítjuk a lista hosszát a lapozáshoz
         if( is_numeric($this->input->post('start')) && is_numeric($this->input->post('length')) ){		
             $params['start'] = $this->input->post('start');
             $params['length'] = $this->input->post('length');
         }

         $params['low_amount'] = '1';
         // Lekérjük az összes találatot
         $count = $this->ProductModel->getProductNum($params);
 
         // Lekérjük a paraméterek alapján a Teáorek listáját és összeállítjuk a választ
         $response = array(
             'data' => $this->ProductModel->getProduct($params),
             'recordsTotal' => $count,
             'recordsFiltered' => $count,			 	
             'post' => $_POST,
             'params' => $params,
             'order' => $order,
         );
         //print_r($response);
 
         // Töröljük a felesleges változókat
         unset($columns);
         unset($search);
         unset($order);
         unset($count);
         unset($params);		
 
 
         // Kiiratjuk a választ
         print json_encode($response);
        }
 
    }

    function inactive_product_list(){


        if($this->input->is_ajax_request()){
            // Beállítjuk az oszlopokat a DataTable-nek
            $columns = array(			
             '0' => 'product_name',				
             '1' => 'product_item_number',
             '2' => 'product_amount',
             '3' => 'product_amount_unit',
             '4' => 'product_min_amount',
             '5' => 'product_status', 
             '6' => 'product_time_created', 
             '7' => 'product_last_edit', 
             '8' => 'product_check', 
         );
 
 
         // Alap tömb a lekérések paraméterezéséhez
         $params = array();
 
         // Kereső kifejezés lekérése
         $search = $this->input->post('search');
         
         // Kereső kifejezés beállítása a lekéréshez
         if( isset($search['value']) && $search['value'] != '' ){
             $params['keyword'] = $search['value'];
         }
 
         // Lekérdezési sorrend beállítása
         $order = $this->input->post('order');
 
         // A DataTable-nek oszlop ID kell, nem név, így a fent deklarált oszlopazonosítókat beállítjuk
         if( isset($order[0]['column']) && isset($order[0]['dir']) && $order[0]['column'] > 1 ){
             $params['order'] = $columns[$order[0]['column']];
             $params['sort'] = $order[0]['dir'];				
         }
 
         // Beállítjuk a lista hosszát a lapozáshoz
         if( is_numeric($this->input->post('start')) && is_numeric($this->input->post('length')) ){		
             $params['start'] = $this->input->post('start');
             $params['length'] = $this->input->post('length');
         }

         //$params['where'] = array('product_status' => '0');
         $params['inactive'] = '1';
         
         // Lekérjük az összes találatot
         $count = $this->ProductModel->getProductNum($params);
 
         // Lekérjük a paraméterek alapján a Teáorek listáját és összeállítjuk a választ
         $response = array(
             'data' => $this->ProductModel->getProduct($params),
             'recordsTotal' => $count,
             'recordsFiltered' => $count,			 	
             'post' => $_POST,
             'params' => $params,
             'order' => $order,
         );
         //print_r($response);
 
         // Töröljük a felesleges változókat
         unset($columns);
         unset($search);
         unset($order);
         unset($count);
         unset($params);		
 
 
         // Kiiratjuk a választ
         print json_encode($response);
        }
 
    }

}