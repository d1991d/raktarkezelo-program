<?php
if ( !defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller {

    function __construct()
	{
        parent::__construct();
        
        $this->load->model('ProductModel');
        $this->load->model('AttributesModel');
        $this->load->model('NotificationModel');
        $this->load->model('MovementModel');

        $content['notifications'] = $this->NotificationModel->notification();

        $this->template = array(
            'header'    => $this->load->view('layout/header', $content, TRUE),
            'content'   => '',
            'sidebar'   => $this->load->view('layout/sidebar', FALSE, TRUE),
            'footer'    => $this->load->view('layout/footer', FALSE, TRUE),
        );

        $this->isAuthentichated = $this->AccountModel->checkLoggedIn();

        $this->logged_in = $this->AccountModel->checkLogIn();

        $this->session->set_userdata('current_url', current_url());

        $this->AccountModel->gotoLockscreen();

        $this->session->set_userdata('click_time', date_create(date('H:i:s')));
    }

    function product_list_data(){


       if($this->input->is_ajax_request()){
           // Beállítjuk az oszlopokat a DataTable-nek
           $columns = array(
            '0' => 'product_select',				
            '1' => 'product_name',				
            '2' => 'product_item_number',
            '3' => 'product_amount',
            '4' => 'product_amount_unit',
            '5' => 'product_min_amount',
            '6' => 'product_status', 
            '7' => 'product_time_created', 
            '8' => 'product_last_edit', 
            '9' => 'product_check', 
        );


        // Alap tömb a lekérések paraméterezéséhez
        $params = array();

        // Kereső kifejezés lekérése
        $search = $this->input->post('search');
        
        // Kereső kifejezés beállítása a lekéréshez
        if( isset($search['value']) && $search['value'] != '' ){
            $params['keyword'] = $search['value'];
        }

        // Lekérdezési sorrend beállítása
        $order = $this->input->post('order');

        // A DataTable-nek oszlop ID kell, nem név, így a fent deklarált oszlopazonosítókat beállítjuk
        if( isset($order[0]['column']) && isset($order[0]['dir']) && $order[0]['column'] > 1 ){
            $params['order'] = $columns[$order[0]['column']];
            $params['sort'] = $order[0]['dir'];				
        }

        // Beállítjuk a lista hosszát a lapozáshoz
        if( is_numeric($this->input->post('start')) && is_numeric($this->input->post('length')) ){		
            $params['start'] = $this->input->post('start');
            $params['length'] = $this->input->post('length');
        }
        // Lekérjük az összes találatot
        $count = $this->ProductModel->getProductNum($params);

        // Lekérjük a paraméterek alapján a Teáorek listáját és összeállítjuk a választ
        $response = array(
            'data' => $this->ProductModel->getProduct($params),
            'recordsTotal' => $count,
            'recordsFiltered' => $count,			 	
            'post' => $_POST,
            'params' => $params,
            'order' => $order,
        );
        //print_r($response);

        // Töröljük a felesleges változókat
        unset($columns);
        unset($search);
        unset($order);
        unset($count);
        unset($params);		


        // Kiiratjuk a választ
        print json_encode($response);
       }

    }

    public function product_list(){

        if($this->isAuthentichated && $this->logged_in){


            $this->template['content']   = $this->load->view('products/product_list',FALSE, TRUE);


            $this->parser->parse('layout/layout', $this->template);

        }

    }


    public function addProduct(){
        
        if($this->isAuthentichated && $this->logged_in){
            
            if($this->input->is_ajax_request()){

                //var_dump($_POST);exit;

                $this->form_validation->set_error_delimiters('', '');
                $this->form_validation->set_rules('product_name', 'product_name', array('required', 'max_length[255]','regex_match[/^[^"%*<>=#$_+&()!\/{}|\';\[\]]+$/]'), array(
                    'required'	    => 'Termék nevének megadása kötelező!',
                    'max_length' 	=> 'A termék neve túl hosszú (Max:255 karakter)!',
                    'regex_match'	=> 'A termék nevének formátuma nem megfelelő. Használható karakterek: kis, nagybetű, szám, szóköz, -\'.',
    
                ));
    
                $this->form_validation->set_rules('product_item_number', 'product_item_number', array('required', 'max_length[255]', 'numeric', 'is_unique[products.product_item_number]'), array(
                    'numeric'	    => 'A cikkszám formátuma nem megfelelő. Csak szám lehet!',
                    'max_length' 	=> 'A cikkszám túl hosszú (Max:255 karakter)!',
                    'required'	    => 'Cikkszám megadása kötelező!',
                    'is_unique'     => 'Ez a cikkszám foglalt!',
                ));
    
                $this->form_validation->set_rules('product_min_amount','product_min_amount', array( 'required', 'is_numeric'), array(
                    'is_numeric'	=> 'A minimum mennyiség csak szám lehet!',
                    'required'	    => 'Minimum mennyiség megadása kötelező!',
                ));
    
                $product_id = $this->input->post('product_id');
                $product_item_number = $this->input->post('product_item_number');

                if($this->form_validation->run() == TRUE){            

                    $data = array(
                        'product_name'          => $this->input->post('product_name'),
                        'product_item_number'   => $this->input->post('product_item_number'),
                        'product_amount_unit'   => $this->input->post('product_amount_unit'),
                        'product_amount'        => '0',
                        'product_min_amount'    => $this->input->post('product_min_amount'),
                        'product_status'        => $this->input->post('product_status'),
                        'product_time_created'  => date('Y-m-d H:i:s'),
                    );

                    $newproduct = $this->ProductModel->addProduct($data);

                    if($newproduct != FALSE) {

                        $log_text = 'Sikeres termék létrehozása: termék id: '.$newproduct;
    
                        $this->CommonModel->log('success', 'products/addProduct', $log_text);

                        $response = array(
                            'type'      => 'success',
                            'title'     => 'Siker',
                            'content'   => 'Sikeresen létrehoztad a terméket!'
                        );

                    }else{

                        $log_text = 'Sikertelen termék létrehozás';
    
                        $this->CommonModel->log('failed', 'products/addProduct', $log_text);

                        $response = array(
                            'type'      => 'save_error',
                            'title'     => 'Hiba',
                            'content'   => 'Hiba a termék létrehozása közben'
                        );
                    }
    
                }else{
                    $response = array(
                        'type'                  => 'error',
                        'product_name'		    => form_error('product_name'),
                        'product_item_number'	=> form_error('product_item_number'),
                        'product_min_amount'	=> form_error('product_min_amount'),
                    );
                }

                print json_encode($response);

            }else{

                $content['attributes'] = $this->AttributesModel->getAttribute();

                $this->template['content']   = $this->load->view('products/new', $content, TRUE);


                $this->parser->parse('layout/layout', $this->template);
            }
        }
    }

    public function deleteProducts() {

        //TODO: meg kell oldalni, hogy a termék ne legyen törölhető, ha már volt rajta mozgás adott évben, vagy van rajta raktárkészlet
        
       if($this->input->is_ajax_request()){

        $product_ids = $this->input->post('productselect');

        $delete = $this->ProductModel->deleteProduct($product_ids);

        if($delete == TRUE){

            $log_text = 'Sikeres termék törlés: termék id(k): '.implode(',', $product_ids);

            $this->CommonModel->log('success', 'products/deleteProducts', $log_text);

            $response = array(
                'type'      => 'success',
                'title'     => 'Törölve!',
                'content'   => 'A termék sikeresen törölve!'
            );
            

        }else{

            $log_text = 'Sikertelen termék törlés: termék id(k): '.implode(',', $product_ids);

            $this->CommonModel->log('failed', 'products/deleteProducts', $log_text);

            $response = array(
                'type'      => 'error',
                'title'     => 'Hiba!',
                'content'   => 'A termék valamilyen hiba miatt nem lett törölve!'
            );
            
        }

        print json_encode($response);

       } 
    }

    public function product($p_id = '') {

        //TODO: megcsinálni, hogy limitálva legyen a lekért adatok száma
        //TODO: Megnézni a getProduct függvényt a ProductModelben, hogy tuti helyesen működik-e
        //TODO: Megcsinálni, hogy a kiszerelési egységre ne az ID-t, hanem a nevét írja ki

        if($this->isAuthentichated && $this->logged_in){
        
            $params = array(
                'where' => array( 'product_id' => $p_id)
            );

            $product_data = $this->ProductModel->getProduct($params);

            if($product_data[0]['product_amount'] > '0'){

                $product_data[0]['disabled'] = 'disabled';
                
            }else{

                $product_data[0]['disabled'] = '';

            }

            $params = array(
                "where"     => array('movement_product_id' => $p_id)
            );

            $movement_datas = $this->MovementModel->getMovement($params);

            $chart_data_1 = '';
            $chart_data_2 = '';

            foreach($movement_datas as $movement_data){

                //$movement_data['movement_date'] = substr($movement_data['movement_date'], 0, 11);

                $chart_data_1 .= $movement_data['movement_new_amount'].',';
                $chart_data_2 .= '"'.$movement_data['movement_date'].'",';


            }

            $chart_data_1 = substr($chart_data_1, 0, -1);
            $chart_data_2 = substr($chart_data_2, 0, -1);

            //pr($chart_data);exit;

            $content = array(
                'product_data'  => $product_data[0],
                'chart_data_1'    => $chart_data_1,
                'chart_data_2'    => $chart_data_2,
            );
            

            $this->template['header']    = $this->load->view('layout/header', FALSE, TRUE);
            $this->template['content']   = $this->load->view('products/product', $content, TRUE);
            $this->template['sidebar']   = $this->load->view('layout/sidebar', FALSE, TRUE); 
            $this->template['footer']    = $this->load->view('layout/footer', FALSE, TRUE);

            $this->parser->parse('layout/layout', $this->template);

        }
    }

    public function editProduct(){

        if($this->input->is_ajax_request()){

            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_rules('product_name', 'product_name', array('required', 'max_length[255]','regex_match[/^[^"%*<>=#$_+&()!\/{}|\';\[\]]+$/]'), array(
                'required'	    => 'Termék nevének megadása kötelező!',
                'max_length' 	=> 'A termék neve túl hosszú (Max:255 karakter)!',
                'regex_match'	=> 'A termék nevének formátuma nem megfelelő. Használható karakterek: kis, nagybetű, szám, szóköz, -\'.',

            ));

            $this->form_validation->set_rules('product_item_number', 'product_item_number', array('required', 'max_length[255]', 'is_numeric'), array(
                //'regex_match'	=> 'A cikkszám formátuma nem megfelelő. Használható karakterek: kis, nagybetű, szám, szóköz, -\'.',
                'is_numeric'    => 'Nem megfelelő karakter, a cikkszám csak szám lehet!',    
                'max_length' 	=> 'A cikkszám túl hosszú (Max:255 karakter)!',
                'required'	    => 'Cikkszám megadása kötelező!',
                //'is_unique' => 'Ez a cikkszám foglalt!',
            ));

            $this->form_validation->set_rules('product_min_amount','product_min_amount', array( 'required', 'is_numeric'), array(
                'is_numeric'	=> 'A minimum mennyiség csak szám lehet!',
                'required'	    => 'Minimum mennyiség megadása kötelező!',
            ));

            $product_id = $this->input->post('product_id');
            $product_item_number = $this->input->post('product_item_number');

            if($this->form_validation->run() == TRUE){            

                if($this->ProductModel->productIdandproductItemNumCheck($product_id, $product_item_number) == FALSE){

                    $log_text = 'Cikkszám már létezik a rendszerben, termék id: '.$product_id;

                    $this->CommonModel->log('success', 'products/edit', $log_text);

                    $response = array(
                        'type'      => 'save_error',
                        'title'     => 'Hiba',
                        'content'   => 'Cikkszám már létezik a rendszerben!'
                    );

                }else{

                    $params = array('where' => array('product_item_number' => $product_item_number));

                    $check_product_item_num = $this->ProductModel->getProduct($params);

                    if($check_product_item_num != FALSE && $check_product_item_num[0]['product_id'] != $product_id){

                        $log_text = 'Cikkszám már létezik a rendszerben, termék id: '.$product_id;

                        $this->CommonModel->log('success', 'products/edit', $log_text);

                        $response = array(
                            'type'      => 'save_error',
                            'title'     => 'Hiba',
                            'content'   => 'Cikkszám már létezik a rendszerben!'
                        );

                    }else{

                        $data = array(
                            'product_name'          => trim($this->input->post('product_name')),
                            'product_item_number'   => trim($this->input->post('product_item_number')),
                            'product_min_amount'    => trim($this->input->post('product_min_amount')),
                            'product_last_edit'     => date('Y-m-d H:i:s'),
                        );
    
                        $edit = $this->ProductModel->editProduct(trim($this->input->post('product_id')), $data);
    
                        if($edit == TRUE){

                            $log_text = 'Sikeres termék szerkesztés, termék id: '.$product_id;

                            $this->CommonModel->log('success', 'products/edit', $log_text);

                            $response = array(
                                'type'      => 'success',
                                'title'     => 'Módosítva',
                                'content'   => 'Sikeresen módosítottad a terméket!'
                            );
    
                        }else{

                            $log_text = 'Hiba a termék szerkesztésekor, termék id: '.$product_id;

                            $this->CommonModel->log('success', 'products/edit', $log_text);
    
                            $response = array(
                                'type'      => 'save_error',
                                'title'     => 'Hiba',
                                'content'   => 'Hiba a szerkesztéskor!'
                            );
    
                        }
                    }
                    
                }

            }else{
                $response = array(
                    'type'                  => 'error',
                    'product_name'		    => form_error('product_name'),
                    'product_item_number'	=> form_error('product_item_number'),
                    'product_min_amount'	=> form_error('product_min_amount'),
                );
            }

            print json_encode($response);
        }
    }

    public function attributes() {

        if($this->isAuthentichated && $this->logged_in){

            $this->template['content']   = $this->load->view('products/attributes',FALSE, TRUE);

            $this->parser->parse('layout/layout', $this->template);


        }
    }

    public function attribute_list_data(){


        if($this->input->is_ajax_request()){
            // Beállítjuk az oszlopokat a DataTable-nek
            $columns = array(
             '0' => 'attribute_select',				
             '1' => 'attribute_shortname',				
             '2' => 'attribute_longname',
             '3' => 'attribute_time_created',
             '4' => 'attribute_last_edit',
             '5' => 'attribute_check', 
         );
 
 
         // Alap tömb a lekérések paraméterezéséhez
         $params = array();
 
         // Kereső kifejezés lekérése
         $search = $this->input->post('search');
         
         // Kereső kifejezés beállítása a lekéréshez
         if( isset($search['value']) && $search['value'] != '' ){
             $params['keyword'] = $search['value'];
         }
 
         // Lekérdezési sorrend beállítása
         $order = $this->input->post('order');
 
         // A DataTable-nek oszlop ID kell, nem név, így a fent deklarált oszlopazonosítókat beállítjuk
         if( isset($order[0]['column']) && isset($order[0]['dir']) && $order[0]['column'] > 1 ){
             $params['order'] = $columns[$order[0]['column']];
             $params['sort'] = $order[0]['dir'];				
         }
 
         // Beállítjuk a lista hosszát a lapozáshoz
         if( is_numeric($this->input->post('start')) && is_numeric($this->input->post('length')) ){		
             $params['start'] = $this->input->post('start');
             $params['length'] = $this->input->post('length');
         }
         // Lekérjük az összes találatot
         $count = $this->AttributesModel->getAttributeNum($params);
 
         // Lekérjük a paraméterek alapján a Teáorek listáját és összeállítjuk a választ
         $response = array(
             'data' => $this->AttributesModel->getAttribute($params),
             'recordsTotal' => $count,
             'recordsFiltered' => $count,			 	
             'post' => $_POST,
             'params' => $params,
             'order' => $order,
         );
         //print_r($response);
 
         // Töröljük a felesleges változókat
         unset($columns);
         unset($search);
         unset($order);
         unset($count);
         unset($params);		
 
 
         // Kiiratjuk a választ
         print json_encode($response);
        }
 
    }

    public function deleteAttributes() {

        //TODO: meg kell oldani, hogy ha már használják vhol, ne legyen törölhető
        
        if($this->input->is_ajax_request()){
 
            $attribute_ids = $this->input->post('attributeselect');

            $delete = $this->AttributesModel->deleteAttribute($attribute_ids);

            if($delete == TRUE){

                $log_text = 'Sikertelen tulajdonság törlés: termék id(k): '.implode(',', $attribute_ids);

                $this->CommonModel->log('failed', 'products/deleteAttributes', $log_text);

                $response = array(
                    'type'      => 'success',
                    'title'     => 'Törölve!',
                    'content'   => 'A tulajdonság sikeresen törölve!'
                );
                

            }else{

                $log_text = 'Sikeres tulajdonság törlés: termék id(k): '.implode(',', $attribute_ids);

                $this->CommonModel->log('success', 'products/deleteAttributes', $log_text);

                $response = array(
                    'type'      => 'error',
                    'title'     => 'Hiba!',
                    'content'   => 'A tulajdonság valamilyen hiba miatt nem lett törölve!'
                );
                
            }

            print json_encode($response);

        } 
    }

    public function attributes_data($attr_id= '') {

        if($this->isAuthentichated && $this->logged_in){

            $params = array(
                'where' => array( 'attribute_id' => $attr_id)
            );

            $attribute_data = $this->AttributesModel->getAttribute($params);

            //var_dump($attribute_data);exit;

            $this->template['content']   = $this->load->view('products/attribute_data', $attribute_data[0], TRUE);

            $this->parser->parse('layout/layout', $this->template);

        }
    }

    public function editAttribute(){

        if($this->input->is_ajax_request()){

            $this->form_validation->set_error_delimiters('', '');
            $this->form_validation->set_rules('attribute_shortname', 'attribute_shortname', array('required', 'max_length[15]','regex_match[/^[^"%*<>=#$_+&()!\/{}|\';\[\]]+$/]'), array(
                'required'	    => 'Tulajdonság rövid nevének megadása kötelező!',
                'max_length' 	=> 'A tulajdonság rövid neve túl hosszú (Max:15 karakter)!',
                'regex_match'	=> 'A termék nevének formátuma nem megfelelő. Használható karakterek: kis, nagybetű, szám, szóköz, -\'.',

            ));

            $this->form_validation->set_rules('attribute_longname', 'attribute_longname', array('required', 'max_length[255]', 'regex_match[/^[^"%*<>=#$_+&()!\/{}|\';\[\]]+$/]'), array(
                'required'	    => 'Tulajdonság hosszú nevének megadása kötelező!',
                'max_length' 	=> 'A tulajdonság hosszú neve túl hosszú (Max:255 karakter)!',
                'regex_match'	=> 'A termék nevének formátuma nem megfelelő. Használható karakterek: kis, nagybetű, szám, szóköz, -\'.',
            ));

            if($this->form_validation->run() == TRUE){

                
                $attr_data = array(
                    'attribute_shortname'   => $this->input->post('attribute_shortname'),
                    'attribute_longname'    => $this->input->post('attribute_longname'),
                    'attribute_last_edit'   => date('Y-m-d H:i:s')
                );

                $attr_edit = $this->AttributesModel->editAttribute($this->input->post('attribute_id'), $attr_data);

                if($attr_edit == TRUE){

                    $log_text = 'Sikeres tulajdonság szerkesztés: termék id: '.$this->input->post('attribute_id');

                    $this->CommonModel->log('success', 'products/editAttributes', $log_text);

                    $response = array(
                        'type'      => 'success',
                        'title'     => 'Módosítva!',
                        'content'   => 'A tulajdonság sikeresen módosítva!'
                    );

                }else{

                    $log_text = 'Sikertelen tulajdonság szerkesztés: termék id: '.$this->input->post('attribute_id');

                    $this->CommonModel->log('failed', 'products/editAttributes', $log_text);

                    $response = array(
                        'type'      => 'error',
                        'title'     => 'Hiba!',
                        'content'   => 'A tulajdonság módosítása sikertelen!'
                    );

                }

            }else{


                $response = array(
                    'type'                  => 'error',
                    'attribute_shortname'	=> form_error('product_name'),
                    'attribute_longname'	=> form_error('attribute_longname'),
                );

            }
            
        
            print json_encode($response);

        }

    }

    public function addAttribute(){

        if($this->isAuthentichated && $this->logged_in){
            
            if($this->input->is_ajax_request()){

                //var_dump($_POST);exit;

                $this->form_validation->set_error_delimiters('', '');
                $this->form_validation->set_rules('attribute_shortname', 'attribute_shortname', array('required', 'max_length[15]','regex_match[/^[^"%*<>=#$_+&()!\/{}|\';\[\]]+$/]'), array(
                    'required'	    => 'Tulajdonság rövid nevének megadása kötelező!',
                    'max_length' 	=> 'A tulajdonság rövid neve túl hosszú (Max:15 karakter)!',
                    'regex_match'	=> 'A termék nevének formátuma nem megfelelő. Használható karakterek: kis, nagybetű, szám, szóköz, -\'.',

                ));

                $this->form_validation->set_rules('attribute_longname', 'attribute_longname', array('required', 'max_length[255]', 'regex_match[/^[^"%*<>=#$_+&()!\/{}|\';\[\]]+$/]'), array(
                    'required'	    => 'Tulajdonság hosszú nevének megadása kötelező!',
                    'max_length' 	=> 'A tulajdonság hosszú neve túl hosszú (Max:255 karakter)!',
                    'regex_match'	=> 'A termék nevének formátuma nem megfelelő. Használható karakterek: kis, nagybetű, szám, szóköz, -\'.',
                ));

                if($this->form_validation->run() == TRUE){
       
                    $attr_data = array(
                        'attribute_shortname'      => $this->input->post('attribute_shortname'),
                        'attribute_longname'       => $this->input->post('attribute_longname'),
                        'attribute_time_created'   => date('Y-m-d H:i:s')
                    );
    
                    $new_attr = $this->AttributesModel->addAttribute($attr_data);
    
                    if($new_attr != FALSE){
    
                        $log_text = 'Sikeres tulajdonság létrehozása: tulajdonság id: '.$new_attr;
    
                        $this->CommonModel->log('success', 'products/addAttribute', $log_text);
    
                        $response = array(
                            'type'      => 'success',
                            'title'     => 'Siker!',
                            'content'   => 'A tulajdonság sikeresen létrehozva!'
                        );
    
                    }else{
    
                        $log_text = 'Sikertelen tulajdonság létrehozás';
    
                        $this->CommonModel->log('failed', 'products/addAttribute', $log_text);
    
                        $response = array(
                            'type'      => 'error',
                            'title'     => 'Hiba!',
                            'content'   => 'A tulajdonság létrehozása sikertelen!'
                        );
    
                    }
    
                }else{
    
    
                    $response = array(
                        'type'                  => 'error',
                        'attribute_shortname'	=> form_error('attribute_shortname'),
                        'attribute_longname'	=> form_error('attribute_longname'),
                    );
    
                }

                print json_encode($response);
            }else{

                $this->template['content']   = $this->load->view('products/newattribute',FALSE, TRUE);

                $this->parser->parse('layout/layout', $this->template);

            }
        }
    }

}