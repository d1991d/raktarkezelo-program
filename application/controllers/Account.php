<?php
if ( !defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {

    function __construct()
	{
        parent::__construct();
        

        $this->isAuthentichated = $this->AccountModel->checkLoggedIn();


        $this->logged_in = $this->AccountModel->checkLogIn();


    }


    public function login(){


        if($this->isAuthentichated == FALSE && $this->logged_in == FALSE){

            if($this->input->is_ajax_request()){
    
                $email = trim($this->input->post('email'));
                $password = trim($this->input->post('pass'));
    
                $this->form_validation->set_error_delimiters('', '');
                $this->form_validation->set_rules('email', 'email', 'required|valid_email',array(
                    'valid_email'   => 'Nem megfelelő e-mail cím formátum!',
                    'required'	    => 'E-mail cím megadása kötelező!',
                ));
    
                $this->form_validation->set_rules('pass', 'pass', 'required',array(
                    'required' =>  'Jelszó megadása kötelező!'
    
                ));
                
                //ha helyesek a megadott adatok
                if ($this->form_validation->run() == TRUE){
    
                    $params = array(
                        'where'     => array('user_email' => $email)
                    );
    
                    $user = $this->AccountModel->getUser($params);
    
                    // var_dump($user);
    
                    if($user != FALSE) {
    
                        $password = $password.$this->config->item('secret_key');
    
                        if(password_verify($password, $user[0]['user_pass'])){
    
                            $data = array(
                                'user_last_login'   => date('Y-m-d H:i:s'),
                            );
    
                            $this->AccountModel->editUser($user[0]['user_id'], $data);
    
                            $user[0]['user_last_login'] = $data['user_last_login'];
    
                            $this->session->set_userdata('user', $user[0]);
                            $this->session->set_userdata('logged_in', 'YES');
                            $this->session->set_userdata('user_id', $user[0]['user_id']);
                            $this->session->set_userdata('click_time', date_create(date('H:i:s')));

                            $log_text = 'Sikeres bejelentkezés, user id: '.$user[0]['user_id'];

                            $this->CommonModel->log('success', 'account/login', $log_text);
    
                            $response = array(
                                'type'      => 'success',
                                'content'   => 'Sikeres bejelentkezés, átirányítás folyamatban...',
                                'url'       => '/fooldal'
        
                            );
                            
                        }else{

                            $log_text = 'Sikertelen bejelentkezés, hibás jelszó, email: '.$email.', jelszó: '.$password;

                            $this->CommonModel->log('failed', 'account/login', $log_text);
    
                            $response = array(
                                'type'      => 'error_log',
                                'content'   => 'Hibás jelszó!'
        
                            );
                        }
    
                    }else{

                        $log_text = 'Sikertelen bejelentkezés, nem regisztrált e-mail cím, email: '.$email.', jelszó: '.$password;

                        $this->CommonModel->log('failed', 'account/login', $log_text);

                        $response = array(
                            'type'      => 'error_log',
                            'content'   => 'Nem regisztrált e-mail cím!'
    
                        ); 
                    }
    
                }else{
    
                    $response = array(
                        'type'      => 'error',
                        'login_email'		=> form_error('email'),
                        'login_password'	=> form_error('pass'),
    
                    );
    
                }
                
                print json_encode($response);
    
            }else{
                $this->load->view('account/login');
            }

        }else{

        }

    }

    public function logout() {

        $user_id = $this->session->userdata('user_id');

        /* Felhasználói és egyéb session adatok törlése	*/
        $this->session->sess_destroy();

        $log_text = 'Sikeres kijelentkezés user id: '.$user_id;

        $this->CommonModel->log('success', 'account/logout', $log_text);

        redirect('/bejelentkezes');
    }


    public function lockScreen() {

        //var_dump($_SESSION, $this->isAuthentichated, $this->logged_in);
        if($this->isAuthentichated == FALSE && $this->logged_in == TRUE){

            if($this->input->is_ajax_request()){

               // var_dump($this->input->post());

                $params = array(

                    'where' => array('user_id' => $this->session->userdata('user_id'))
    
                );
    
                $user = $this->AccountModel->getUser($params);        

                $password = $this->input->post('password').$this->config->item('secret_key');


                if(password_verify($password, $user[0]['user_pass'])){
    
                    $data = array(
                        'user_last_login'   => date('Y-m-d H:i:s'),
                    );

                    $this->AccountModel->editUser($user[0]['user_id'], $data);

                    $user[0]['user_last_login'] = $data['user_last_login'];

                    $this->session->set_userdata('user', $user[0]);
                    $this->session->set_userdata('logged_in', 'YES');
                    $this->session->set_userdata('user_id', $user[0]['user_id']);
                    $this->session->set_userdata('click_time', date_create(date('H:i:s')));

                    $log_text = 'Sikeres bejelentkezés lockscreenről: user_id: '.$user[0]['user_id'];

                    $this->CommonModel->log('success', 'account/lockscreen', $log_text);

                    $response = array(
                        'type'      => 'success',
                        'content'   => 'Sikeres bejelentkezés, átirányítás folyamatban...',
                        'url'       => $this->session->userdata('current_url')

                    );
                    
                }else{

                    $log_text = 'Sikertelen bejelentkezés lockscreenről: user_id: '.$user[0]['user_id'].' jelszó: '.$this->input->post('password');

                    $this->CommonModel->log('failed', 'account/lockscreen', $log_text);

                    $response = array(
                        'type'      => 'error',
                        'content'   => 'Hibás jelszó!',
                    );

                }

                print json_encode($response);
            }else{
                $params = array(

                    'where' => array('user_id' => $this->session->userdata('user_id'))
    
                );
    
                $userdata = $this->AccountModel->getUser($params);

                $log_text = 'Kijelentkezés lockscreenre: user_id: '.$userdata[0]['user_id'];

                $this->CommonModel->log('success', 'account/lockscreen', $log_text);
    
                $this->load->view('account/lockscreen', $userdata[0]);
            }

        }

    }



}