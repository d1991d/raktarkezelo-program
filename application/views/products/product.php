<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Termék lista
        <small>Raktárkészlet</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Termékek</a></li>
        <li class="active"><a href="#">Termék lista</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3 hidden-xs"></div>
        <div class="col-md-6 col-xs-12">
          <!-- /.box -->
            <div class="box">
                <div class="box-header with-border">
                <h3 class="box-title"><b><?= upCase($product_data['product_name']) ?></b> adatok</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?= form_open('', 'id="product_edit_form" method="post"') ?>
                    <input type="hidden" name="product_id" value="<?= $this->uri->segment(2) ?>">
                      <div class="form-group" id="product_name_error">
                          <label for="product_name">Termék neve</label>
                          <input type="text" class="form-control" name="product_name" id="product_name" placeholder="Termék neve" value="<?= $product_data['product_name'] ?>">
                          <span class="help-block"></span>
                        </div>
                      <div class="form-group" id="product_item_number_error">
                          <label for="product_item_number">Termék cikkszám</label>
                          <input type="text" class="form-control" name="product_item_number" id="product_item_number" placeholder="Cikkszám" value="<?= $product_data['product_item_number'] ?>">
                          <span class="help-block"></span>
                        </div>
                      <div class="form-group">
                          <label for="product_amount">Jelenlegi raktárkészlet</label>
                          <input type="text" class="form-control" name="product_amount" id="product_amount" placeholder="Darabszám" value="<?= $product_data['product_amount'] ?>" disabled>
                          <span class="help-block"></span>
                        </div>
                      <div class="form-group">
                          <label for="product_amount_unit">Kiszerelési egység</label>
                          <input type="text" class="form-control" name="product_amount_unit" id="product_amount_unit" placeholder="Kiszerelési egység" value="<?= $product_data['product_amount_unit'] ?>" disabled>
                          <span class="help-block"></span>
                        </div>
                      <div class="form-group" id="product_min_amount_error">
                          <label for="product_min_amount">Minimum mennyiség</label>
                          <a href="#" data-toggle="tooltip_1" title="Ha a raktárkészlet a minimum mennyiség alá vagy közelébe csökken, a főoldalon jelzésre kerül"><i class="fa fa-fw fa-question-circle"></i></a>
                          <input type="text" class="form-control" name="product_min_amount" id="product_min_amount" placeholder="Minimum mennyiség" value="<?= $product_data['product_min_amount'] ?>">
                          <span class="help-block"></span>
                        </div>
                      <div class="form-group">
                            <label for="activation">Termék állapota</label>
                            <a href="#" data-toggle="tooltip_2" title="Itt kikapcsolhatod a terméket, ha nincs rajta épp készlet. Ha van, nem lehetséges inaktiválni!"><i class="fa fa-fw fa-question-circle"></i></a>
                            <select class="form-control" name="product_status" <?= $product_data['disabled'] ?>>
                                <option>Aktív</option>
                                <option>Inaktív</option>
                            </select>
                      </div>
                      <div class="form-group">
                        <button type="button" id="product_edit" class="btn btn-success">Szerkesztés</button>
                      </div>
                    <?= form_close() ?>
                </div>
                <!-- /.box-body -->
        </div>
        <div class="col-md-3 hidden-xs"></div>
      </div>

      <div class="col-xs-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Raktárkészlet mozgás</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                      title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="chart">
            <canvas id="chart" height="400" style="max-height: 500px; width: auto"></canvas>
            </div>
          </div>
          <!-- /.box-body -->
          <div class="box-footer">
          Raktárkészlet mozgás
          </div>
          <!-- /.box-footer-->
        </div>
      </div>
    </div>
  </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
    $(document).ready(function(){
         $('[data-toggle="tooltip_1"]').tooltip();   
         $('[data-toggle="tooltip_2"]').tooltip();  

        var canvas = document.getElementById('chart');
        var ctx = canvas.getContext('2d');
    			var myChart = new Chart(ctx, {
        		type: 'line',
		        data: {
		            labels: [<?php print_r($chart_data_2); ?>],
		            datasets: 
		            [{
		                label: 'Mennyiség',
		                data: [<?php print_r($chart_data_1); ?>],
		                backgroundColor: 'transparent',
		                borderColor:'rgba(255,99,132)',
		                borderWidth: 3
		            }]
		        }
          });

         $('#product_edit').on('click', function(){
          Swal.fire({
            title: 'Biztosan módosítod?',
            text: "A módosítás mentésre kerül! Később újra módosíthatod a terméket",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Igen!',
            cancelButtonText: 'Nem'
          }).then((result) => {
            if (result.value) {
              console.log(result)
              $.ajax({  
                type: 'POST',  
                url: '/products/editProduct',
                data: $('#product_edit_form').serialize(),
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    if(data.type == 'error'){
                        for(var key in data){
                            console.log(data);
                            if(data[key] !== ''){
                                $('#' + key +'_error').addClass('has-error');
                                $('#' + key + '_error > span').text(data[key]);
                            }else {
                                $('#' + key +'_error').removeClass('has-error');
                                $('#' + key + '_error > span').text('');
                                
                            }
                        }
                    }
                    if(data.type == "save_error"){
                      $('#product_name_error').removeClass('has-error');
                        $('#product_item_number_error').removeClass('has-error');
                        $('#product_min_amount_error').removeClass('has-error');
                        $('#product_name_error > span').text('');
                        $('#product_item_number_error > span').text('');
                        $('#product_min_amount_error > span').text('');
                        Swal.fire({
                          title: data.title,
                          text: data.content,
                          type: 'warning',
                        });
                    }
                    if(data.type == 'success'){
                        $('#product_name_error').removeClass('has-error');
                        $('#product_item_number_error').removeClass('has-error');
                        $('#product_min_amount_error').removeClass('has-error');
                        $('#product_name_error > span').text('');
                        $('#product_item_number_error > span').text('');
                        $('#product_min_amount_error > span').text('');
                        Swal.fire(
                            data.title,
                            data.content,
                            'success'
                        )
                    }
                },
                error: function(error){
                    console.log(error);
                }         
              });  
            }
          })
        })
    });


</script>
