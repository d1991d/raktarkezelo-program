  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Termék lista
        <small>Raktárkészlet</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Termékek</a></li>
        <li class="active"><a href="#">Termék lista</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-3 hidden-xs"></div>
        <div class="col-md-6 col-xs-12">
          <!-- /.box -->
            <div class="box">
                <div class="box-header with-border">
                <h3 class="box-title"><b>Termék</b> adatok</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?= form_open('', 'id="product_edit_form" method="post"') ?>
                      <div class="form-group" id="product_name_error">
                          <label for="product_name">Termék neve</label>
                          <input type="text" class="form-control" name="product_name" id="product_name" placeholder="Termék neve">
                          <span class="help-block"></span>
                        </div>
                      <div class="form-group" id="product_item_number_error">
                          <label for="product_item_number">Termék cikkszám</label>
                          <input type="text" class="form-control" name="product_item_number" id="product_item_number" placeholder="Cikkszám">
                          <span class="help-block"></span>
                        </div>
                      <div class="form-group">
                            <label for="product_amount_unit">Kiszerelési egység</label>
                            <select class="form-control" name="product_amount_unit">
                                <?php
                                    foreach($attributes as $attribute){
                                        print'<option value="'.$attribute['attribute_id'].'">'.$attribute['attribute_shortname'].'</option>';
                                    }
                                ?>
                            </select>
                            <span class="help-block"></span>
                        </div>
                      <div class="form-group" id="product_min_amount_error">
                          <label for="product_min_amount">Minimum mennyiség</label>
                          <a href="#" data-toggle="tooltip_1" title="Ha a raktárkészlet a minimum mennyiség alá vagy közelébe csökken, a főoldalon jelzésre kerül"><i class="fa fa-fw fa-question-circle"></i></a>
                          <input type="text" class="form-control" name="product_min_amount" id="product_min_amount" placeholder="Minimum mennyiség" >
                          <span class="help-block"></span>
                        </div>
                      <div class="form-group">
                            <label for="activation">Termék állapota</label>
                            <a href="#" data-toggle="tooltip_2" title="Itt kikapcsolhatod a terméket, ha nincs rajta épp készlet. Ha van, nem lehetséges inaktiválni!"><i class="fa fa-fw fa-question-circle"></i></a>
                            <select class="form-control" name="product_status">
                                <option value="1">Aktív</option>
                                <option value="0">Inaktív</option>
                            </select>
                      </div>
                      <div class="form-group">
                        <button type="button" id="product_edit" class="btn btn-success">Mentés</button>
                      </div>
                    <?= form_close() ?>
                </div>
                <!-- /.box-body -->
        </div>
        <div class="col-md-3 hidden-xs"></div>
    <!-- /.content -->
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
    $(document).ready(function(){
         $('[data-toggle="tooltip_1"]').tooltip();   
         $('[data-toggle="tooltip_2"]').tooltip();  

         $('#product_edit').on('click', function(){
          Swal.fire({
            title: 'Biztosan létrehozod?',
            text: "A terméket később szerkesztheted",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Igen!',
            cancelButtonText: 'Nem'
          }).then((result) => {
            if (result.value) {
              console.log(result)
              $.ajax({  
                type: 'POST',  
                url: '/products/addProduct',
                data: $('#product_edit_form').serialize(),
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    if(data.type == 'error'){
                        for(var key in data){
                            console.log(data);
                            if(data[key] !== ''){
                                $('#' + key +'_error').addClass('has-error');
                                $('#' + key + '_error > span').text(data[key]);
                            }else {
                                $('#' + key +'_error').removeClass('has-error');
                                $('#' + key + '_error > span').text('');
                                
                            }
                        }
                    }
                    if(data.type == "save_error"){
                        $('#product_name_error').removeClass('has-error');
                        $('#product_item_number_error').removeClass('has-error');
                        $('#product_min_amount_error').removeClass('has-error');
                        $('#product_name_error > span').text('');
                        $('#product_item_number_error > span').text('');
                        $('#product_min_amount_error > span').text('');
                        Swal.fire({
                          title: data.title,
                          text: data.content,
                          type: 'error',
                        });
                    }
                    if(data.type == 'success'){
                        $('#product_name_error').removeClass('has-error');
                        $('#product_item_number_error').removeClass('has-error');
                        $('#product_min_amount_error').removeClass('has-error');
                        $('#product_name_error > span').text('');
                        $('#product_item_number_error > span').text('');
                        $('#product_min_amount_error > span').text('');
                        Swal.fire(
                            data.title,
                            data.content,
                            'success'
                        )
                    }
                },
                error: function(error){
                    console.log(error);
                }         
              });  
            }
          })
        })
    });
</script>
