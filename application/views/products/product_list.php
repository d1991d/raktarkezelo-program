  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Termék lista
        <small>Raktárkészlet</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Termékek</a></li>
        <li class="active"><a href="#">Termék lista</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->
            <div class="box">
                <div class="box-header">
                <h3 class="box-title">Termékek listája</h3>
                </div>
                <div style="float:right; margin:0 10px 10px 0">
                    <a href="/uj_termek" class="btn btn-success action-buttons delete-button datalink" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="új termék">
                        <i class="fa fa-fw fa-save"></i>
                    </a>
                    <button href="/product/delete" class="btn btn-danger action-buttons delete-button datalink" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Törlés" id="delete" >
                        <i class="fa fa-fw fa-trash"></i>
                    </button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <?= form_open('', 'id="product_list_form" method="post"')?>
                    <table id="product_list_table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Kiválasztás</th>
                            <th>Név</th>
                            <th>Cikkszám</th>
                            <th>Darabszám</th>
                            <th>Kiszerelési egység</th>
                            <th>Minimum mennyiség</th>
                            <th>Állapot</th>
                            <th>Reg. dátum</th>
                            <th>Módosítás dátuma</th>                      
                            <th>Megtekint</th>
                        </tr>
                        </thead>
                    </table>
                    <?= form_close();?>
                </div>
                <!-- /.box-body -->
            </div>
    <!-- /.content -->
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>

    $(document).change('.check', function() {
        if ($('.check:checked').length) {
            $('.btn-danger').removeAttr('disabled');
        } else {
            $('.btn-danger').attr('disabled', 'disabled');
        }
    });

    $(document).ready(function(){

        $('.btn-danger').attr('disabled', 'disabled');

        $('#product_list_table').DataTable( {
            'lengthMenu': <?= json_encode($this->config->item('datatable_list'))?>,
            'processing': true,
            'serverSide': true,
            'searching' : true,
            'searchDelay' : false,
            'autoWidth': false,
            'scrollX': true,
            'dom':    "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            'ajax': {
            'destroy': true,
            'url': '/products/product_list_data',
            'type': 'POST',
            'data': $('#product_list_form').serializeObject(),
            },
            'initComplete': function() {
                $('.dataTables_filter input').unbind();
                $('.dataTables_filter input').bind('keyup input', function(e){
                    e.preventDefault();
                    var self = this;
                    clearTimeout(this.searchTimer);
                    this.searchTimer = setTimeout(function() {$('#product_list_table').DataTable().search(self.value).draw();}, 800);
                    var code = e.keyCode || e.which;
                    if (code == 13) {
                    clearTimeout(this.searchTimer);
                    $('#product_list_table').DataTable().search(this.value).draw();
                    }
                });
                $('.dataTables_filter input').bind('keypress', function(e){
                    var code = e.keyCode || e.which;
                    if (code == 13) {
                    return false;
                    }
                });
                },
                'columns': [
                {'data': 'product_select'},			
                {'data': 'product_name'},			
                {'data': 'product_item_number'},
                {'data': 'product_amount'},
                {'data': 'product_amount_unit'},
                {'data': 'product_min_amount'},
                {'data': 'product_status'},
                {'data': 'product_time_created'},
                {'data': 'product_last_edit'},
                {'data': 'product_check'},
                ],
                'columnDefs': [
                {'targets': 0, 'className': 'text-center checkbox-container', 'bSortable': false},
                {'targets': 1, 'className': 'text-center profil-container',  'bSortable': true},
                {'targets': 2, 'className': 'text-center profil-container',  'bSortable': false},
                {'targets': 3, 'className': 'text-center profil-container',  'bSortable': false},
                {'targets': 4, 'className': 'text-center profil-container',  'bSortable': false},
                {'targets': 5, 'className': 'text-center profil-container',  'bSortable': false},
                {'targets': 6, 'className': 'text-center profil-container',  'bSortable': true},
                {'targets': 7, 'className': 'text-center profil-container',  'bSortable': true},
                {'targets': 8, 'className': 'text-center profil-container',  'bSortable': false},
                {'targets': 9, 'className': 'text-center profil-container',  'bSortable': false},
                ],
                'language': {
                'sEmptyTable':     'Nincs rendelkezésre álló adat',
                'sInfo':           'Találatok: _START_ - _END_ Összesen: _TOTAL_',
                'sInfoEmpty':      'Nulla találat',
                'sInfoFiltered':   '(_MAX_ összes rekord közül szűrve)',
                'sInfoPostFix':    '',
                'sInfoThousands':  ' ',
                'sLengthMenu':     'Megjelenítve:  _MENU_',
                'sLoadingRecords': 'Betöltés...',
                'sProcessing':     'Feldolgozás...',
                'sSearch':         'Keresés:',
                'sZeroRecords':    'Nincs a keresésnek megfelelő találat',
                'oPaginate': {
                    'sFirst':    'Első',
                    'sPrevious': 'Előző',
                    'sNext':     'Következő',
                    'sLast':     'Utolsó'
                },
                'oAria': {
                    'sSortAscending':  ': aktiválja a növekvő rendezéshez',
                    'sSortDescending': ': aktiválja a csökkenő rendezéshez'
                }
                },
                'order' : [[8, 'asc']]
        } );

        $('.btn-danger').on('click', function(){

            Swal.fire({
            title: 'Biztosan törlöd?',
            text: "A művelet nem visszavonható!",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Igen!',
            cancelButtonText: 'Nem'
            }).then((result) => {
                if (result.value) {
                    //console.log(result)
                    $.ajax({  
                        type: 'POST',  
                        url: '/products/deleteProducts',
                        data: $('#product_list_form').serialize(),
                        dataType: 'json',
                        success: function(data) {
                            if(data.type == 'success'){
                                Swal.fire(
                                    data.title,
                                    data.content,
                                    'success'
                                )
                                $('#product_list_table').DataTable().ajax.reload();
                            }else{
                                Swal.fire(
                                    data.title,
                                    data.content,
                                    'error'
                                )
                            }
                        },
                        error: function(error){
                            console.log(error);
                            
                        }         
                    });
                }
            })
            
        })
    
    })
</script>
                 