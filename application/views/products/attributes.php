  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tulajdonság lista
        <small>Raktárkészlet</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Tulajdonságok</a></li>
        <li class="active"><a href="/tulajdonsag_lista">Tulajdonság lista</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->
            <div class="box">
                <div class="box-header">
                <h3 class="box-title">Tulajdonságok listája</h3>
                </div>
                <div style="float:right; margin:0 10px 10px 0">
                <a href="/uj_tulajdonsag" class="btn btn-success action-buttons delete-button datalink" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Új tulajdonság">
                        <i class="fa fa-fw fa-save"></i>
                </a>
                <button href="/product/attribute_delete" class="btn btn-danger action-buttons delete-button datalink" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Törlés" id="delete">
                     <i class="fa fa-fw fa-trash"></i>
                 </button>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?= form_open('', 'id="attribute_list_form" method="post"')?>
                        <table id="attribute_list_table" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Kiválasztás</th>
                                <th>Rövid neve</th>
                                <th>Teljes név</th>
                                <th>Reg. dátum</th>
                                <th>Módosítás dátuma</th>                      
                                <th>Megtekint</th>
                            </tr>
                            </thead>
                        </table>
                    <?= form_close();?>
                </div>
                <!-- /.box-body -->
            </div>
    <!-- /.content -->
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script>

        $(document).change('.check', function() {
            if ($('.check:checked').length) {
                $('.btn-danger').removeAttr('disabled');
            } else {
                $('.btn-danger').attr('disabled', 'disabled');
            }
        });
    $(document).ready(function(){

        $('.btn-danger').attr('disabled', 'disabled');

        $('#attribute_list_table').DataTable( {
            'lengthMenu': <?= json_encode($this->config->item('datatable_list'))?>,
            'processing': true,
            'serverSide': true,
            'searching' : true,
            'searchDelay' : false,
            'autoWidth': false,
            'scrollX': true,
            'dom':    "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            'ajax': {
            'destroy': true,
            'url': '/products/attribute_list_data',
            'type': 'POST',
            'data': $('#attribute_list_form').serializeObject(),
            },
            'initComplete': function() {
                $('.dataTables_filter input').unbind();
                $('.dataTables_filter input').bind('keyup input', function(e){
                    e.preventDefault();
                    var self = this;
                    clearTimeout(this.searchTimer);
                    this.searchTimer = setTimeout(function() {$('#attribute_list_table').DataTable().search(self.value).draw();}, 800);
                    var code = e.keyCode || e.which;
                    if (code == 13) {
                    clearTimeout(this.searchTimer);
                    $('#attribute_list_table').DataTable().search(this.value).draw();
                    }
                });
                $('.dataTables_filter input').bind('keypress', function(e){
                    var code = e.keyCode || e.which;
                    if (code == 13) {
                    return false;
                    }
                });
                },
                'columns': [
                {'data': 'attribute_select'},			
                {'data': 'attribute_shortname'},			
                {'data': 'attribute_longname'},
                {'data': 'attribute_time_created'},
                {'data': 'attribute_last_edit'},
                {'data': 'attribute_check'},
                ],
                'columnDefs': [
                {'targets': 0, 'className': 'text-center checkbox-container', 'bSortable': false},
                {'targets': 1, 'className': 'text-center profil-container',  'bSortable': true},
                {'targets': 2, 'className': 'text-center profil-container',  'bSortable': true},
                {'targets': 3, 'className': 'text-center profil-container',  'bSortable': true},
                {'targets': 4, 'className': 'text-center profil-container',  'bSortable': false},
                {'targets': 5, 'className': 'text-center profil-container',  'bSortable': false},
                ],
                'language': {
                'sEmptyTable':     'Nincs rendelkezésre álló adat',
                'sInfo':           'Találatok: _START_ - _END_ Összesen: _TOTAL_',
                'sInfoEmpty':      'Nulla találat',
                'sInfoFiltered':   '(_MAX_ összes rekord közül szűrve)',
                'sInfoPostFix':    '',
                'sInfoThousands':  ' ',
                'sLengthMenu':     'Megjelenítve:  _MENU_',
                'sLoadingRecords': 'Betöltés...',
                'sProcessing':     'Feldolgozás...',
                'sSearch':         'Keresés:',
                'sZeroRecords':    'Nincs a keresésnek megfelelő találat',
                'oPaginate': {
                    'sFirst':    'Első',
                    'sPrevious': 'Előző',
                    'sNext':     'Következő',
                    'sLast':     'Utolsó'
                },
                'oAria': {
                    'sSortAscending':  ': aktiválja a növekvő rendezéshez',
                    'sSortDescending': ': aktiválja a csökkenő rendezéshez'
                }
                },
                'order' : [[2, 'asc']]
        } );

        $('#delete').on('click', function(){

            Swal.fire({
            title: 'Biztosan törlöd?',
            text: "A művelet nem visszavonható!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Igen!',
            cancelButtonText: 'Nem'
            }).then((result) => {
                if (result.value) {
                    console.log(result)
                    $.ajax({  
                        type: 'POST',  
                        url: '/products/deleteAttributes',
                        data: $('#attribute_list_form').serialize(),
                        dataType: 'json',
                        success: function(data) {
                            if(data.type == 'success'){
                                Swal.fire(
                                    data.title,
                                    data.content,
                                    'success'
                                )
                                $('#attribute_list_table').DataTable().ajax.reload();
                            }else{
                                Swal.fire(
                                    data.title,
                                    data.content,
                                    'error'
                                )
                            }
                        },
                        error: function(error){
                            console.log(error);
                            
                        }         
                    });
                }
            })
            
        })


    
    })
  </script>
                 