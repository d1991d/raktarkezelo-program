  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Termék lista
        <small>Raktárkészlet</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Termékek</a></li>
        <li class="active"><a href="#">Termék lista</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-3 hidden-xs"></div>
        <div class="col-md-6 col-xs-12">
          <!-- /.box -->
            <div class="box">
                <div class="box-header with-border">
                <h3 class="box-title"><b>Tulajdonság</b> adatok</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?= form_open('', 'id="attribute_edit_form" method="post"') ?>
                      <div class="form-group" id="attribute_shortname_error">
                          <label for="attribute_shortname">Tulajdonság neve</label>
                          <input type="text" class="form-control" name="attribute_shortname" id="attribute_shortname" placeholder="Tulajdonság neve">
                          <span class="help-block"></span>
                        </div>
                         <div class="form-group" id="attribute_longname_error">
                          <label for="attribute_longname">Tulajdonság teljes neve</label>
                          <input type="text" class="form-control" name="attribute_longname" id="attribute_longname" placeholder="Tulajdonság teljes neve">
                          <span class="help-block"></span>
                        </div>
                      <div class="form-group">
                        <button type="button" id="attribute_edit" class="btn btn-success">Mentés</button>
                      </div>
                    <?= form_close() ?>
                </div>
                <!-- /.box-body -->
        </div>
        <div class="col-md-3 hidden-xs"></div>
    <!-- /.content -->
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>


    $(document).ready(function(){
         $('[data-toggle="tooltip_1"]').tooltip();   
         $('[data-toggle="tooltip_2"]').tooltip();  

         $('#attribute_edit').on('click', function(){
          Swal.fire({
            title: 'Biztosan létrehozod?',
            text: "A terméket később szerkesztheted",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Igen!',
            cancelButtonText: 'Nem'
          }).then((result) => {
            if (result.value) {
              console.log(result)
              $.ajax({  
                type: 'POST',  
                url: '/products/addattribute',
                data: $('#attribute_edit_form').serialize(),
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    if(data.type == 'error'){
                        for(var key in data){
                            console.log(data);
                            if(data[key] !== ''){
                                $('#' + key +'_error').addClass('has-error');
                                $('#' + key + '_error > span').text(data[key]);
                            }else {
                                $('#' + key +'_error').removeClass('has-error');
                                $('#' + key + '_error > span').text('');
                                
                            }
                        }
                    }
                    if(data.type == "save_error"){

                      $('#attribute_shortname_error').removeClass('has-error');
                      $('#attribute_longname_error').removeClass('has-error');
                      $('#attribute_shortname_error > span').text('');
                      $('#attribute_longname_error > span').text('');
                      Swal.fire({
                        title: data.title,
                        text: data.content,
                        type: 'warning',
                      });

                    }
                    if(data.type == 'success'){

                      $('#attribute_shortname_error').removeClass('has-error');
                      $('#attribute_longname_error').removeClass('has-error');
                      $('#attribute_shortname_error > span').text('');
                      $('#attribute_longname_error > span').text('');
                      Swal.fire(
                          data.title,
                          data.content,
                          'success'
                      )
                      
                    }
                },
                error: function(error){
                    console.log(error);
                }         
              });  
            }
          })
        })
    });
</script>
