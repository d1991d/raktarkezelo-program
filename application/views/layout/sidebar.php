<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    <div class="user-panel">
        <div class="pull-left image">
          <img src="/img/default_profile_img.webp" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?//= ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENÜPONTOK</li>
        <!-- Optionally, you can add icons to the links -->
        <li><a href="/fooldal"><i class="fa fa-link"></i> <span>Főoldal</span></a></li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i><span>Termékek</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/termek_lista"><i class="fa fa-circle-o"></i> Termék lista</a>
            <li><a href="/tulajdonsag_lista"><i class="fa fa-circle-o"></i> Termék tulajdonságok</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-line-chart"></i> <span>Termék mozgás</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/bevetelezes"><i class="fa fa-circle-o"></i> Bevételezés</a></li>
            <li><a href="/kiadas"><i class="fa fa-circle-o"></i> Kiadás</a></li>
            <!-- <li><a href="lists.php"><i class="fa fa-circle-o"></i> Lista lekérések</a></li> -->
            <li><a href="/sztorno"><i class="fa fa-circle-o"></i> Bevét/kiadás sztornózása</a></li>
          </ul>
        </li>
        <li><a href="/adatbazis"><i class="fa fa-save"></i> <span>Adatbázis mentés</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>