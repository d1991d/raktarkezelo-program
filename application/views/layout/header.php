<header class="main-header">

<!-- Logo -->
<a href="index2.html" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><b>M</b>EN</span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg"><b>M</b>ENÜ</span>
</a>

<!-- Header Navbar -->
<nav class="navbar navbar-static-top" role="navigation">
  <!-- Sidebar toggle button-->
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>
  <!-- Navbar Right Menu -->
  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">
    <li class="dropdown notifications-menu">
      <?php if($notifications['notification_num'] > 0 ? $label = 'danger' : $label = 'success'); ?>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-<?= $label ?>"><?= $notifications['notification_num'] ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header"><?= $notifications['notification_num'] ?> értesítésed van</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <?php

                      if(is_array($notifications['notifications'])){
                        foreach($notifications['notifications'] as $key => $value){

                          print'
                            <li>
                              <a href="'.$value[0].'">
                                <i class="fa fa-warning text-yellow"></i>'.$value[1].'
                              </a>
                            </li>
                          ';
                        }
                      }else{
                        print'
                            <li>
                              <a href="#"><i class="fa fa-fw fa-thumbs-o-up"></i>'.$notifications['notifications'].'</a>
                            </li>
                          ';
                      }
                     
                  ?>

                 
                  <li class="footer"><a href="#"><?= $notifications['notification_num'] ?> értesítésed van</a></li>
                </ul>
              </li>
              
            </ul>
          </li>
      <!-- User Account Menu -->
      <li class="dropdown user user-menu">
        <!-- Menu Toggle Button -->
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="/img/default_profile_img.webp" class="user-image" alt="User Image">
          <!-- hidden-xs hides the username on small devices so only the image appears. -->
          <span><?= $_SESSION['user']['user_username']?></span>
        </a>
        <ul class="dropdown-menu">
          <!-- The user image in the menu -->
          <li class="user-header">
          <img src="/img/default_profile_img.webp" class="img-circle" alt="User Image">
            <p>
            <?//=?>
              <small><?//=?></small>
            </p>
          </li>
          <!-- Menu Body -->
          <!-- Menu Footer-->
          <li class="user-footer">           
            <div class="pull-right">
              <a href="/kijelentkezes" class="btn btn-default btn-flat">Kijelentkezés</a>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
</header>
