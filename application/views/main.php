  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Főoldal
        <!-- <small>it all starts here</small> -->
      </h1>
      <ol class="breadcrumb">
        <li><a href="/fooldal"><i class="fa fa-dashboard"></i> Főoldal</a></li>
      </ol>
    </section>

    <section class="content">

      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3><?= $sum_product_num ?><sup style="font-size: 20px">db</sup></h3>

            <p>Összes termékek száma</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="/termek_lista" class="small-box-footer">Ugrás a termékekre &nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green">
          <div class="inner">
            <h3><?= $active_product_num ?><sup style="font-size: 20px">db</sup></h3>

            <p>Aktív termékek száma</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="/termek_lista" class="small-box-footer">Ugrás a termékekre &nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3><?= $low_amount_product_num ?><sup style="font-size: 20px">db</sup></h3>

            <p>Alacsony raktárkészletű  termékek száma</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="/fooldal#low_amount_product_list" class="small-box-footer">Ugrás az alacsony raktárkészletű termékek listájára &nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>

      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red">
          <div class="inner">
            <h3><?= $inactive_product_num ?><sup style="font-size: 20px">db</sup></h3>

            <p>Inaktív termékek száma</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="/fooldal#inactive_product_list" class="small-box-footer">Ugrás az inaktív termékek listájára &nbsp;&nbsp; <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Alacsony raktárkészletű termékek listája</h3>

        </div>
        <div class="box-body">
            <?= form_open('', 'id="low_amount_product_list" method="post"')?>
              <table id="low_amount_product_list_table" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                      <th>Név</th>
                      <th>Cikkszám</th>
                      <th>Darabszám</th>
                      <th>Kiszerelési egység</th>
                      <th>Minimum mennyiség</th>
                      <th>Állapot</th>
                      <th>Reg. dátum</th>
                      <th>Módosítás dátuma</th>                      
                      <th>Megtekint</th>
                  </tr>
                  </thead>
              </table>
            <?= form_close();?>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        Alacsony raktárkészletű termékek listája
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Inaktív termékek listája</h3>
          
        </div>
        <div class="box-body">
            <?= form_open('', 'id="inactive_product_list" method="post"')?>
              <table id="inactive_product_list_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Név</th>
                    <th>Cikkszám</th>
                    <th>Darabszám</th>
                    <th>Kiszerelési egység</th>
                    <th>Minimum mennyiség</th>
                    <th>Állapot</th>
                    <th>Reg. dátum</th>
                    <th>Módosítás dátuma</th>                      
                    <th>Megtekint</th>
                </tr>
                </thead>
              </table>
            <?= form_close();?>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        Inaktív termékek listája
        </div>
        <!-- /.box-footer-->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script>
  
    $(document).ready(function(){
      $('#low_amount_product_list_table').DataTable( {
        'lengthMenu': <?= json_encode($this->config->item('datatable_list'))?>,
        'processing': true,
        'serverSide': true,
        'searching' : true,
        'searchDelay' : false,
        'autoWidth': false,
        'scrollX': true,
        'dom':    "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        'ajax': {
        'destroy': true,
        'url': '/main/low_amount_product_list',
        'type': 'POST',
        'data': $('#low_amount_product_list').serializeObject(),
        },
        'initComplete': function() {
          $('#low_amount_product_list_table_filter input').unbind();
          $('#low_amount_product_list_table_filter input').bind('keyup input', function(e){
              e.preventDefault();
              var self = this;
              clearTimeout(this.searchTimer);
              this.searchTimer = setTimeout(function() {$('#low_amount_product_list_table').DataTable().search(self.value).draw();}, 800);
              var code = e.keyCode || e.which;
              if (code == 13) {
              clearTimeout(this.searchTimer);
              $('#low_amount_product_list_table').DataTable().search(this.value).draw();
              }
          });
          $('#low_amount_product_list_table_filter input').bind('keypress', function(e){
              var code = e.keyCode || e.which;
              if (code == 13) {
              return false;
              }
          });
          },
          'columns': [	
          {'data': 'product_name'},			
          {'data': 'product_item_number'},
          {'data': 'product_amount'},
          {'data': 'product_amount_unit'},
          {'data': 'product_min_amount'},
          {'data': 'product_status'},
          {'data': 'product_time_created'},
          {'data': 'product_last_edit'},
          {'data': 'product_check'},
          ],
          'columnDefs': [
          {'targets': 0, 'className': 'text-center checkbox-container', 'bSortable': false},
          {'targets': 1, 'className': 'text-center profil-container',  'bSortable': true},
          {'targets': 2, 'className': 'text-center profil-container',  'bSortable': false},
          {'targets': 3, 'className': 'text-center profil-container',  'bSortable': false},
          {'targets': 4, 'className': 'text-center profil-container',  'bSortable': false},
          {'targets': 5, 'className': 'text-center profil-container',  'bSortable': false},
          {'targets': 6, 'className': 'text-center profil-container',  'bSortable': true},
          {'targets': 7, 'className': 'text-center profil-container',  'bSortable': true},
          {'targets': 8, 'className': 'text-center profil-container',  'bSortable': false},
          ],
          'language': {
          'sEmptyTable':     'Nincs rendelkezésre álló adat',
          'sInfo':           'Találatok: _START_ - _END_ Összesen: _TOTAL_',
          'sInfoEmpty':      'Nulla találat',
          'sInfoFiltered':   '(_MAX_ összes rekord közül szűrve)',
          'sInfoPostFix':    '',
          'sInfoThousands':  ' ',
          'sLengthMenu':     'Megjelenítve:  _MENU_',
          'sLoadingRecords': 'Betöltés...',
          'sProcessing':     'Feldolgozás...',
          'sSearch':         'Keresés:',
          'sZeroRecords':    'Nincs a keresésnek megfelelő találat',
          'oPaginate': {
              'sFirst':    'Első',
              'sPrevious': 'Előző',
              'sNext':     'Következő',
              'sLast':     'Utolsó'
          },
          'oAria': {
              'sSortAscending':  ': aktiválja a növekvő rendezéshez',
              'sSortDescending': ': aktiválja a csökkenő rendezéshez'
          }
          },
          'order' : [[6, 'asc']]
      });

      $('#inactive_product_list_table').DataTable( {
        'lengthMenu': <?= json_encode($this->config->item('datatable_list'))?>,
        'processing': true,
        'serverSide': true,
        'searching' : true,
        'searchDelay' : false,
        'autoWidth': false,
        'scrollX': true,
        'dom':    "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
        'ajax': {
        'destroy': true,
        'url': '/main/inactive_product_list',
        'type': 'POST',
        'data': $('#inactive_product_list').serializeObject(),
        },
        'initComplete': function() {
          $('#inactive_product_list_table_filter input').unbind();
          $('#inactive_product_list_table_filter input').bind('keyup input', function(e){
              e.preventDefault();
              var self = this;
              clearTimeout(this.searchTimer);
              this.searchTimer = setTimeout(function() {$('#inactive_product_list_table').DataTable().search(self.value).draw();}, 800);
              var code = e.keyCode || e.which;
              if (code == 13) {
              clearTimeout(this.searchTimer);
              $('#inactive_product_list_table').DataTable().search(this.value).draw();
              }
          });
          $('#inactive_product_list_table_filter input').bind('keypress', function(e){
              var code = e.keyCode || e.which;
              if (code == 13) {
              return false;
              }
          });
          },
          'columns': [	
          {'data': 'product_name'},			
          {'data': 'product_item_number'},
          {'data': 'product_amount'},
          {'data': 'product_amount_unit'},
          {'data': 'product_min_amount'},
          {'data': 'product_status'},
          {'data': 'product_time_created'},
          {'data': 'product_last_edit'},
          {'data': 'product_check'},
          ],
          'columnDefs': [
          {'targets': 0, 'className': 'text-center checkbox-container', 'bSortable': false},
          {'targets': 1, 'className': 'text-center profil-container',  'bSortable': true},
          {'targets': 2, 'className': 'text-center profil-container',  'bSortable': false},
          {'targets': 3, 'className': 'text-center profil-container',  'bSortable': false},
          {'targets': 4, 'className': 'text-center profil-container',  'bSortable': false},
          {'targets': 5, 'className': 'text-center profil-container',  'bSortable': false},
          {'targets': 6, 'className': 'text-center profil-container',  'bSortable': true},
          {'targets': 7, 'className': 'text-center profil-container',  'bSortable': true},
          {'targets': 8, 'className': 'text-center profil-container',  'bSortable': false},
          ],
          'language': {
          'sEmptyTable':     'Nincs rendelkezésre álló adat',
          'sInfo':           'Találatok: _START_ - _END_ Összesen: _TOTAL_',
          'sInfoEmpty':      'Nulla találat',
          'sInfoFiltered':   '(_MAX_ összes rekord közül szűrve)',
          'sInfoPostFix':    '',
          'sInfoThousands':  ' ',
          'sLengthMenu':     'Megjelenítve:  _MENU_',
          'sLoadingRecords': 'Betöltés...',
          'sProcessing':     'Feldolgozás...',
          'sSearch':         'Keresés:',
          'sZeroRecords':    'Nincs a keresésnek megfelelő találat',
          'oPaginate': {
              'sFirst':    'Első',
              'sPrevious': 'Előző',
              'sNext':     'Következő',
              'sLast':     'Utolsó'
          },
          'oAria': {
              'sSortAscending':  ': aktiválja a növekvő rendezéshez',
              'sSortDescending': ': aktiválja a csökkenő rendezéshez'
          }
          },
          'order' : [[6, 'asc']]
      });
    })
  //TODO: meg kell oldani, hogy az inaktív termékek listájának táblázata is jól keressen
  </script>

  