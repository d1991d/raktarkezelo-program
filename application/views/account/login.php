


<!DOCTYPE html>
<html lang=HU>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Raktár program verzió: <?= $this->config->item('version') ?> </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="/css/skin-blue.min.css">

    <link rel="stylesheet" href="/css/daterangepicker.css">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css">


    <!-- my style -->
    <link href="/css/style.css" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script> -->


    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
</head>






<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>Raktárkezelő </b>program</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Bejelentkezés</p>

            <form method="post" id="login">
                <span class="help-block" id="success" style="text-align:center; color: green"></span>
                <div class="alert alert-danger alert-dismissible" style="display:none" id="error_log">
                    <p></p>
                </div>
                <div class="form-group has-feedback" id="login_email_error">
                    <input type="email" id="email" name="email" class="form-control" placeholder="E-mail cím..">
                    <span class="help-block"></span>
                </div>
                
                <div class="form-group has-feedback" id="login_password_error">
                    <input type="password" name="pass" id="pass" class="form-control" placeholder="Jelszó..">
                    <span class="help-block"></span>
                </div>
                <div class="row">
                    <div class="col-xs-7">
                      <a href="#">Elfelejtettem a jelszavam</a><br>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-5">
                    <button type="button" name="submit" class="btn btn-primary btn-block btn-flat">Bejelentkezés</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <!-- /.social-auth-links -->

            

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    <script src="/js/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="/js/bootstrap.min.js"></script>
    
</body>

</html>


<script>
    $(document).ready(function(){

$('.btn-primary').on('click', function(){
    $.ajax({  
        type: 'POST',  
        url: 'account/login',
        data: $('#login').serialize(),
        dataType: 'json',
        success: function(data) {
            console.log(data);
            if(data.type == 'error'){
                for(var key in data){
                    console.log(data);
                    if(data[key] !== ''){
                        $('#' + key +'_error').addClass('has-error');
                        $('#' + key + '_error > span').text(data[key]);
                    }else {
                        $('#' + key +'_error').removeClass('has-error');
                        $('#' + key + '_error > span').text('');
                        
                    }
                }
            }
            if(data.type == 'error_log'){
                $('#login_email_error').removeClass('has-error');
                $('#login_password_error').removeClass('has-error');
                $('#login_email_error > span').text('');
                $('#login_password_error > span').text('');
                //$('#error_log > p').text(data.content);
               // $('#error_log').css('display', 'block');
              /* toastr["error"](data.content)
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }*/
                iziToast.error({
                    timeout: 3000,
                    resetOnHover: true,
                    transitionIn: 'flipInX',
                    transitionOut: 'flipOutX',
                    position: 'topRight',
                    title: 'OK',
                    message: data.content,
                }); 
            }
            if(data.type == 'success'){
                $('#login_email_error').removeClass('has-error');
                $('#login_password_error').removeClass('has-error');
                $('#login_email_error > span').text('');
                $('#login_password_error > span').text('');
                $('#error_log > p').text('');
                $('#error_log').css('display', 'none')
                /*toastr["success"](data.content)
                toastr.options = {
                    "closeButton": false,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }*/
                iziToast.success({
                    timeout: 2000,
                    resetOnHover: true,
                    transitionIn: 'flipInX',
                    transitionOut: 'flipOutX',
                    position: 'topRight',
                    title: 'OK',
                    message: data.content,
                }); 
                //$('#success').text(data.content);

                // Hide it after 3 seconds
                setTimeout(function(){
                    window.location.href= data.url;
                }, 2500);
            }
        },
        error: function(error){
            console.log(error);
        }         
    });
})
})
</script>