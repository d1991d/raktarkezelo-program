<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Lockscreen</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/css/AdminLTE.min.css">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition lockscreen">
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a href="/fooldal"><b><?= $this->config->item('page_name') ?></b></a>
  </div>
  <!-- User name -->
    <div class="lockscreen-name"><?= $user_username ?>
        <br><span class="lockscreen-name log_success"></span>
        <br><span class="lockscreen-name password_error"></span>
    </div>

  <!-- START LOCK SCREEN ITEM -->
  <div class="lockscreen-item">
    <!-- lockscreen image -->
    <div class="lockscreen-image">
      <img src="/img/default_profile_img.webp" alt="User Image">
    </div>
    <!-- /.lockscreen-image -->

    <!-- lockscreen credentials (contains the form) -->
    <form class="lockscreen-credentials">
      <div class="input-group">
        <input type="password" id="pass" class="form-control" placeholder="Jelszó..">


        <div class="input-group-btn">
          <button type="button" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
        </div>
      </div>
    </form>
    <!-- /.lockscreen credentials -->

  </div>
  <!-- /.lockscreen-item -->
  <div class="help-block text-center">
    Add meg a jelszavad, hogy visszaléphess az oldalra
  </div>
  <div class="text-center">
    <a href="/bejelentkezes">Bejelentkezés másként</a>
  </div>
  <div class="lockscreen-footer text-center">
    Copyright &copy; <?= $this->config->item('create_date') ?> <b><a href="#" class="text-black">Nagy Dávid</a></b><br>
    All rights reserved v<?= $this->config->item('version') ?>
  </div>
</div>
<!-- /.center -->

<script src="/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/js/bootstrap.min.js"></script>

<script>
    $(document).on('click', '.btn', function(){

        $.ajax({  
            type: 'POST',  
            url: 'account/lockscreen',
            data: {
                password: $('#pass').val(),
            },  
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if(data.type == 'error'){
                    //$('.password_error').text(data.content);
                 /* toastr["error"](data.content)
                  toastr.options = {
                      "closeButton": false,
                      "debug": false,
                      "newestOnTop": false,
                      "progressBar": false,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": false,
                      "onclick": null,
                      "showDuration": "300",
                      "hideDuration": "1000",
                      "timeOut": "5000",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                  }*/
                  iziToast.error({
                    timeout: 3000,
                    resetOnHover: true,
                    transitionIn: 'flipInX',
                    transitionOut: 'flipOutX',
                    position: 'topRight',
                    title: 'OK',
                    message: data.content,
                  });
                }
                if(data.type == 'success'){
                    //$('.log_success').text(data.content);
                  /*toastr["success"](data.content)
                  toastr.options = {
                      "closeButton": false,
                      "debug": false,
                      "newestOnTop": false,
                      "progressBar": false,
                      "positionClass": "toast-top-right",
                      "preventDuplicates": false,
                      "onclick": null,
                      "showDuration": "300",
                      "hideDuration": "1000",
                      "timeOut": "5000",
                      "extendedTimeOut": "1000",
                      "showEasing": "swing",
                      "hideEasing": "linear",
                      "showMethod": "fadeIn",
                      "hideMethod": "fadeOut"
                  }*/
                    iziToast.success({
                      timeout: 2000,
                      resetOnHover: true,
                      transitionIn: 'flipInX',
                      transitionOut: 'flipOutX',
                      position: 'topRight',
                      title: 'OK',
                      message: data.content,
                    }); 
                    // Hide it after 3 seconds
                    setTimeout(function(){
                        window.location.href= data.url;
                    }, 2500);
                }
            },
            error: function(error){
                console.log(error);
            }         
        });
    })
</script>
</body>
</html>