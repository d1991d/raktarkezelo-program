<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Bevételezés - / kiadás sztornózása
        <small>Bevételezés - / kiadás sztornózása</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mozgások</a></li>
        <li><a href="/sztorno">Bevételezés - / kiadás sztornózása</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Sztornó varázsló</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-3 hidden-xs"></div>
          <div class="col-xs-12 col-md-6">
              <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Sztornózás</h3>
                </div>
                <div class="box-body">
                  <div class="form-group">
                        <label for="movement_type">Bizonylat típusa</label>
                        <select class="form-control" id="movement_type" name="movement_type">
                          <option>Válassz!</option>
                          <option value='0'>Bevételezés</option>
                          <option value='1'>Kiadás</option>
                        </select>
                    </div>
                    <div class="form-group" id="form_id_code_div">

                    </div>
                    <div id="products">
                      <?= form_open('', 'id="product_list" method="post"') ?>
                      <div class="form-group" id="product_list">

                      </div>
                      <?= form_close() ?>
                    </div>
                </div>
                <div class="box-footer" id="buttons">
                    <button type="button" id="storno" class="btn btn-danger">Törlés</button>
                </div>
              </div>
          </div>
          <div class="col-md-3 hidden-xs"></div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        Sztornó varázsló
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>

  <script>

      $(document).ready(function(){

        //Törlés gomb alap helyzetben
        $('#storno').css('display', 'none')

        //bizonylat típus kiválasztása
        $('#movement_type').on('change', function(){

            $('#createPdf').remove();
            $('#storno_id_code').remove();

            $.ajax({  
              type: 'POST',  
              url: '/stock/stornoType',
              data: {
                  movement_type: $(this).val(),
              },
              dataType: 'json',
              success: function(data) {
              
                if(data.type == 'success'){
                  $('#form_id_code_div').html('');
                  $('#form_id_code_div').html(data.content);
                }

              },
              error: function(error){
                  console.log(error);
              }         
            }); 
        })
      })

      
      // livesearch kereső
      $(document).on('keyup', '#form_id_code', function(){
            $.ajax({  
                type: 'POST',  
                url: '/stock/form_id_codeforStorno',
                data: {
                    search: $('#form_id_code').val(),
                },
                dataType: 'json',
                success: function(data) {
                    $('#response').html(data.data);

                },
                error: function(error){
                    console.log(error);
                }         
            }); 
        })

    // bizonylat kiválasztása
    $(document).on('click', '.response > li', function(e){

        //console.log($(this).data(value));
        $('#storno').css('display', 'none')
        $.ajax({  
            type: 'POST',  
            url: '/stock/selectForm',
            data: {
                search: $(this).data('value'),//$(this).text(),
            },
            dataType: 'json',
            success: function(data) {

                if(data.type == 'success'){
                    $('.response').remove();
                    $('#form_id_code').val(data.movement_id_code);
                    $('#product_list').html(data.data);
                    $('#storno').css('display', 'block')
                }

                if(data.type == 'error'){
                  $('.response').remove();
                  $('#product_list').html(data.data);
                }

            },
            error: function(error){
                console.log(error);
            }         
        });

    });

    //Törlés
    $(document).on('click', '#storno', function(){
        Swal.fire({
        title: 'Biztonsan sztornózni akarod?',
        text: "Ha most törlöd a tételt a bizonylatról, később már nem vonhatod vissza!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Igen',
        cancelButtonText: 'Vissza',
        }).then((result) => {
        if (result.value) {
            $.ajax({  
                type: 'POST',  
                url: '/stock/storno',
                data: $('#product_list').serialize(),
                dataType: 'json',
                success: function(data) {

                  if(data.type == 'success'){
                      $('.response').remove(); // livesearch lista
                      $('#form_id_code_div').empty(''); // bizonylat szám input mező
                      $('#product_list_select').remove(); // select box
                      $('#products label').remove(); // select box label-je
                      $('#movement_type').val($("#movement_type option:first").val()); //bizonylat típus
                      $('#storno').css('display', 'none'); // "Törlés" gomb

                      Swal.fire({
                        title: data.title,
                        html: data.content,
                        type: 'success',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Igen, szeretnék'
                      }).then((result) => {
                        if (result.value) {

                        if(data.type == 'success'){

                          $('#buttons').append(data.button);

                          Swal.fire({
                            title: data.title,
                            html: data.content,
                            type: 'success',
                          });

                        }else{

                          Swal.fire({
                            title: data.title,
                            text: data.content,
                            type: 'error',
                          });

                        }
                        
                        }
                      })

                  }else{
                    Swal.fire(
                          data.title,
                          data.content,
                          'error'
                      )
                  }

                },
                error: function(error){
                    console.log(error);
                }         
            });
        }
      })
    })

    $(document).on('click', '#createPdf', function(){

      $(this).attr('disabled', 'disabled');
      $(this).html('Készül...  <i class="fa fa-spinner fa-spin"></i>');

      setTimeout(function(){
        $.ajax({  
          type: 'POST',  
          url: '/stock/stornoPdf',
          data: {
            storno_id_code: $('#storno_id_code').val(),
            form_type: 3,
          },
          dataType: 'json',
          success: function(data) {
            if(data.type == 'success'){

              $('#createPdf').attr('disabled', false);
              $('#createPdf').html('Nyomtatvány készítése');

              Swal.fire({
                title: data.title,
                text: data.content, 
                type: 'success',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Megnyitás'
              }).then((result) => {
                if (result.value) {
                  window.open(data.url)
                }
              })
            
            }

          },
          error: function(error){
              console.log(error);
          }         
        });
      }, 3000);

    })

  </script>