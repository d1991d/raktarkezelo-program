 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mozgások
        <small>Raktárkészlet mozgás</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mozgások</a></li>
        <li><a href="/mozgasok">Raktárkészlet mozgás adott termékre</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Raktárkészlet mozgás adott termékre</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-3 hidden-xs"></div>
          <div class="col-md-6 col-xs-12">
            <?= form_open('', 'id="movement_form" method="post"')?>
                <div class="form-group">
                  <label for="product_name">Termék neve</label>
                  <input type="text" class="form-control" id="product_name" name="product_name">
                  <div id="response"></div>
                </div>
                <div class="form-group">
                  <label for="product_item_number">Cikkszám</label>
                  <input type="text" class="form-control" id="product_item_number" name="product_item_number" value='<?= $product['product_item_number'] ?>' readonly>
                </div>
                <div class="form-group">
                  <label for="product_amount">Aktuális darabszám</label>
                  <input type="number" class="form-control" id="product_amount" name="product_amount" value="<?= $product['product_amount'] ?>" disabled>
                </div>
                <div class="form-group">
                  <label for="product_amount">Kiszerelési egység</label>
                  <input type="text" class="form-control" id="product_amount_unit" name="product_amount_unit" readonly>
                </div>
                <div class="form-group" id="movement_amount_error">
                  <label for="movement_amount">Módosítás értéke</label>
                  <input type="number" class="form-control" id="movement_amount" name="movement_amount" placeholder="Módosítás értéke">
                  <span class="help-block"></span>
                </div>
                <div class="form-group" id="new_amount_error">
                  <label for="new_amount">Új darabszám a változtatás után</label>
                  <input type="number" class="form-control" id="new_amount" name="new_amount" placeholder="Új darabszám a változtatás után.." disabled>
                  <span class="help-block"></span>
                </div>

                <button class="btn btn-primary" type="button" id="save" >Kiadás</button>


            <?= form_close()?>

            <div class="col-xs-12 text-center" id="pdfbutton">

            </div>
          </div>
          <div class="col-md-3 hidden-xs"></div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script>

 $(document).ready(function(){
  $( "#product_name" ).keyup(function(){
    $.ajax({  
        type: 'POST',  
        url: '/stock/livesearch',
        data: {
            search: $('#product_name').val(),
        },
        dataType: 'json',
        success: function(data) {
            $('#response').html(data.data);
            $('#movement_amount').val('0');

        },
        error: function(error){
            console.log(error);
        }         
    }); 
  })
 })

$(document).on('click', '.response > li', function(e){

    //console.log($(this).val());
    $.ajax({  
      type: 'POST',  
      url: '/stock/selectProduct',
      data: {
          product_id: $(this).val(),
      },
      dataType: 'json',
      success: function(data) {

          if(data.type == 'success'){

              $('#response').html('');
              $('#new_amount').val('0');
              $('#product_name').val(data.product_name);
              $('#product_item_number').val(data.product_item_number);
              $('#product_amount').val(data.product_amount);
              $('#product_amount_unit').val(data.product_amount_unit);
          }

      },
      error: function(error){
          console.log(error);
      }         
    });

  });

      $(document).ready(function(){
          $('#movement_amount').keyup(function(){

            $('#new_amount').val(parseInt($('#product_amount').val())  - parseInt($('#movement_amount').val()));

          });

      })

      $('#save').on('click', function(){
        $('#movement_amount_error').removeClass('has-error');
        $('#new_amount_error').removeClass('has-error');
        $('#new_amount_error > span').text('');
        $('#movement_amount_error').addClass('');

        if($('#movement_amount').val() > 0 && $('#new_amount').val() > 0){
          Swal.fire({
            title: 'Biztosan végrehajtod?',
            text: "A mentés után már csak sztornózni tudod a mozgást. A mozgásról a mentés után bizonylatot is készíthetsz.",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Igen!',
            cancelButtonText: 'Nem'
          }).then((result) => {
            if (result.value) {
              console.log(result)
              $.ajax({  
                type: 'POST',  
                url: '/stock/release',
                data: $('#movement_form').serialize(),
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    if(data.type == 'success'){
                        $('#movement_amount_error').removeClass('has-error');
                        $('#movement_amount_error > span').text('');

                        Swal.fire({
                          title: data.title,
                          html: data.content,
                          type: 'success',
                          showCancelButton: true,
                          confirmButtonColor: '#3085d6',
                          cancelButtonColor: '#d33',
                          confirmButtonText: 'Igen, szeretnék'
                        }).then((result) => {
                          if (result.value) {

                            if(data.type == 'success'){

                              $('#products').empty();
                              $('#pdfbutton').append(data.button);

                              Swal.fire({
                                title: data.title,
                                html: data.content,
                                type: 'success',
                              });

                            }else{

                              Swal.fire({
                                title: data.title,
                                text: data.content,
                                type: 'error',
                              });

                            }
                            
                          }
                        })


                    }else{
                        Swal.fire({
                          title: data.title,
                          text: data.content,
                          type: 'error',
                        });

                        if(data.class == '1'){
                            $('#movement_amount_error').addClass('has-error');
                            $('#movement_amount_error > span').text(data.content);

                        }else{
                            $('#movement_amount_error').removeClass('has-error');
                            $('#movement_amount_error > span').text('');
                        }
                    }
                    
                },
                error: function(error){
                    console.log(error);
                }         
              });  
            }
          })
        }else{

            if($('#new_amount').val() < 0){

            $('#new_amount_error').addClass('has-error');
            $('#new_amount_error > span').text('Az raktárkészlet nem lehet kisebb, mint 0!');

            }
            if($('#movement_amount').val() <= 0){
            
            $('#movement_amount_error').addClass('has-error');
            $('#movement_amount_error > span').text('A kiadás értéke nem lehet 0!');1
            }

            iziToast.warning({
                timeout: 2000,
                resetOnHover: true,
                transitionIn: 'flipInX',
                transitionOut: 'flipOutX',
                position: 'topRight',
                title: 'OK',
                message: 'Hozzáadás sikertelen!',
            }); 
        }

      })

      $(document).ready(function(){
        $(document).on('click', '#createPdf', function(){

          $(this).attr('disabled', 'disabled');
          $(this).html('Készül...  <i class="fa fa-spinner fa-spin"></i>');

          setTimeout(function(){
            $.ajax({  
              type: 'POST',  
              url: '/stock/pdf',
              data: {
                movement_id_code: $('#movement_id_code').val(),
                form_type: 2,
              },
              dataType: 'json',
              success: function(data) {
                if(data.type == 'success'){

                  $('#createPdf').attr('disabled', false);
                  $('#createPdf').html('Nyomtatvány készítése');
                  $('#products').empty();

                  Swal.fire({
                    title: data.title,
                    text: data.content, 
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Megnyitás'
                  }).then((result) => {
                    if (result.value) {
                      window.open(data.url)
                    }
                  })
                
                }

              },
              error: function(error){
                  console.log(error);
              }         
            });
          }, 3000);

        })
      })
  </script>