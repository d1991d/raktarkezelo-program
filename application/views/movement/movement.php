 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mozgások
        <small>Raktárkészlet mozgás</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mozgások</a></li>
        <li><a href="/mozgasok">Bevételezés</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Bevételezés</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-3 hidden-xs"></div>
          <div class="col-md-6 col-xs-12">
            <?= form_open('', 'id="movement_form" method="post"')?>

                <div class="form-group">
                    <label for="livesearch">Keresés</label>
                    <input type="text" class="form-control" id="livesearch" placeholder="Termék vagy cikkszám">
                    <div id="response"></div>
                </div>
                <div class="form-group">
                  <label for="product_name">Termék neve</label>
                  <input type="text" class="form-control" id="product_name" name="product_name" disabled>
                </div>
                <div class="form-group">
                  <label for="product_item_number">Cikkszám</label>
                  <input type="text" class="form-control" id="product_item_number" name="product_item_number" readonly>
                </div>
                <div class="form-group">
                  <label for="product_amount">Aktuális darabszám</label>
                  <input type="number" class="form-control" id="product_amount" name="product_amount" disabled>
                </div>
                <div class="form-group">
                  <label for="product_amount">Kiszerelési egység</label>
                  <input type="text" class="form-control" id="product_amount_unit" disabled>
                </div>
                <div class="form-group" id="movement_amount_error">
                  <label for="movement_amount">Módosítás értéke</label>
                  <input type="number" class="form-control" id="movement_amount" name="movement_amount" placeholder="Módosítás értéke">
                  <span class="help-block"></span>
                </div>
                <div class="form-group">
                  <label for="new_amount">Új darabszám a változtatás után</label>
                  <input type="number" class="form-control" id="new_amount" name="new_amount" placeholder="Új darabszám a változtatás után.." disabled>
                </div>
                <button class="btn btn-primary" type="button" id="add" >Hozzáad</button>
            <?= form_close()?>

            <div class="col-xs-12 text-center" id="pdfbutton">

            </div>
          </div>
          <div class="col-md-3 hidden-xs"></div>
        </div>
      <!-- /.box-body -->
        <div class="box-footer">
          Bevételezés
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Bevételezési lista</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <?= form_open('', 'id="product_list_form" method="post"')?>
            
            <table class="table table-bordered table-hover dataTable" style="max-height: 35px; overflow-y: auto">
                <thead>
                    <tr>
                        <th>Termék név</th>
                        <th>Termék cikkszám</th>
                        <th>Termék régi darabszám</th>
                        <th>Kiszerelési egység</th>
                        <th>Növekedési mennyiség</th>
                        <th>Kiszerelési egység</th>
                        <th>Termék új darabszám</th>
                        <th>Kiszerelési egység</th>
                        <th>Törlés</th>
                    </tr>
                </thead>
                <tbody id="products">
                </tbody>
            </table>

            <button class="btn btn-success" style="margin-top: 30px" type="button" id="save" >Bevételezés</button>
          <?= form_close()?>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Bevételezési lista
        </div>
        <!-- /.box-footer-->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script>

      $(document).ready(function(){

        $( "#livesearch" ).keyup(function(){
            $.ajax({  
                type: 'POST',  
                url: '/stock/livesearch',
                data: {
                    search: $('#livesearch').val(),
                },
                dataType: 'json',
                success: function(data) {
                    $('#response').html(data.data);
                    $('#movement_amount').val('0');

                },
                error: function(error){
                    console.log(error);
                }         
            }); 
        })

        $(document).on('click', '.response > li', function(e){

            //console.log($(this).val());
            $.ajax({  
                type: 'POST',  
                url: '/stock/selectProduct',
                data: {
                    product_id: $(this).val(),
                },
                dataType: 'json',
                success: function(data) {

                    if(data.type == 'success'){

                        $('#response').html('');
                        $('#new_amount').val('0');
                        $('#product_name').val(data.product_name);
                        $('#product_item_number').val(data.product_item_number);
                        $('#product_amount').val(data.product_amount);
                        $('#product_amount_unit').val(data.product_amount_unit);
                    }

                },
                error: function(error){
                    console.log(error);
                }         
            });
        
        });

        $('#movement_amount').keyup(function(){

          $('#new_amount').val(parseInt($('#movement_amount').val()) + parseInt($('#product_amount').val()));

        });

        $('#add').on('click', function(){

            if($('#movement_amount').val() != '' || $('#movement_amount').val() > 0){

              $('#movement_amount_error').removeClass('has-error');
              $('#movement_amount_error > span').text('');

              $.ajax({  
                type: 'POST',  
                url: '/stock/addProduct',
                data: $('#movement_form').serialize(),
                dataType: 'json',
                success: function(data) {

                  if(data.type == 'success'){

                      $('#products').append(data.content);
                  
                  }

                },
                error: function(error){
                    console.log(error);
                }         
              });

            }else{
              $('#movement_amount_error').addClass('has-error');
              $('#movement_amount_error > span').text('Bevételezés értéke nagyobb kell, hogy legyen, mint 0!');

              iziToast.warning({
                  timeout: 2000,
                  resetOnHover: true,
                  transitionIn: 'flipInX',
                  transitionOut: 'flipOutX',
                  position: 'topRight',
                  title: 'OK',
                  message: 'Hozzáadás sikertelen!',
              }); 
            }
            
        })

        $('#save').on('click', function(){

          Swal.fire({
            title: 'Biztosan végrehajtod?',
            text: "A mentés után már csak sztornózni tudod a mozgást. A mozgásról a mentés után bizonylatot is készíthetsz.",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Igen!',
            cancelButtonText: 'Nem'
          }).then((result) => {
            if (result.value) {
              $.ajax({  
                type: 'POST',  
                url: '/stock/saveMovement',
                data: $('#product_list_form').serialize(),
                dataType: 'json',
                success: function(data) {

                  if(data.type == 'success'){

                    $('#products').empty();
                    $('#pdfbutton').append(data.button);

                    Swal.fire({
                      title: data.title,
                      html: data.content,
                      type: 'success',
                    });
                  
                  }else{

                    Swal.fire({
                      title: data.title,
                      text: data.content,
                      type: 'error',
                    });

                  }

                },
                error: function(error){
                    console.log(error);
                }         
              });
            }
          })

        })

        $(document).on('click', '#createPdf', function(){

          $(this).attr('disabled', 'disabled');
          $(this).html('Készül...  <i class="fa fa-spinner fa-spin"></i>');

          setTimeout(function(){
            $.ajax({  
              type: 'POST',  
              url: '/stock/pdf',
              data: {
                movement_id_code: $('#movement_id_code').val(),
                form_type: 1,
              },
              dataType: 'json',
              success: function(data) {
                if(data.type == 'success'){

                  $('#createPdf').attr('disabled', false);
                  $('#createPdf').html('Nyomtatvány készítése');
                  $('#products').empty();
  
                  Swal.fire({
                    title: data.title,
                    text: data.content, 
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Megnyitás'
                  }).then((result) => {
                    if (result.value) {
                      window.open(data.url)
                    }
                  })
                
                }

              },
              error: function(error){
                  console.log(error);
              }         
            });
          }, 3000);

        })


        $('#product_list_form').on('click', "#delete", function(){
            $(this).closest("tr").remove();
        })

      })

  </script>