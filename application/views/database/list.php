  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Adatbázis
        <small>lista</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Adatbázis</a></li>
        <li class="active"><a href="#">Adatbázis lista</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
          <div class="col-md-3 hidden-xs"></div>
        <div class="col-md-6 col-xs-12">
          <!-- /.box -->
            <div class="box">
                <div class="box-header with-border">
                <h3 class="box-title"><b>Adatbázis</b> adatok</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <h4 style="text-align: center">Adatbázis mentéséhez kattints a "Mentés" gombra</h4>
                    <div class="box-footer text-center">
                        <button type="button" id="database_save" class="btn btn-primary">Mentés</button>
                    </div>
                    <br><br>
                    <div style="position: relative; height: 320px; overflow: auto; display: block;">
                      <table id="example2" class="table table-bordered table-hover">
                          <thead>
                              <tr>
                                  <th>Adatbázis mentések listája</th>
                              </tr>
                          </thead>
                          <tbody> 
                              <?php 
                                    
                                  if(!empty($databases)){

                                      foreach ($databases as $list) { 
                                          
                                          print'
                                          <tr>
                                              <td>'.$list.'</td>
                                          </tr>
                                          ';
                                      }
                                      
                                  }else{

                                      print'
                                      <tr>
                                          <td>Nincs adatbázis mentésed!</td>
                                      </tr>';
                                  }
                                    
                                ?>
                          </tbody>
                          <tfoot>
                              <tr>
                                  <th>Adatbázis mentések listája</th>
                              </tr>
                          </tfoot>
                      </table>
                    </div>
                </div>
                <!-- /.box-body -->
        </div>
        <div class="col-md-3 hidden-xs"></div>
    <!-- /.content -->
    </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script>
    $(document).ready(function(){
         $('[data-toggle="tooltip_1"]').tooltip();   
         $('[data-toggle="tooltip_2"]').tooltip();  

         $('#database_save').on('click', function(){
          Swal.fire({
            title: 'Biztosan létrehozod?',
            text: "Lehetőség szerint sűrűn készíts adatbázis mentést!",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Igen!',
            cancelButtonText: 'Nem'
          }).then((result) => {
            if (result.value) {
              console.log(result)
              $.ajax({  
                type: 'POST',  
                url: '/database/saveDB',
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    if(data.type == 'success'){
                        Swal.queue([{
                            title: data.title,
                            confirmButtonText: 'OK',
                            text: data.content,                            
                            type: 'success',
                            showLoaderOnConfirm: false,
                            preConfirm: () => {
                                location.reload();
                            }
                        }])

                    }else{
                        Swal.fire({
                          title: data.title,
                          text: data.content,
                          type: 'error',
                        });
                    }
                    
                },
                error: function(error){
                    console.log(error);
                }         
              });  
            }
          })
        })
    });
</script>
