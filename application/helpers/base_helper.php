<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * Language helper
 *
 * A bekért érték visszaadása a config.php-ban
 * beállított nyelvnek megfelelően.
 *
 * @access	public
 * @return	string
 *
 */


function is_non_unique($str, $field)
{
	$resp = FALSE;

	$CI =& get_instance();
	sscanf($field, '%[^.].%[^.]', $table, $field);
	if( isset($CI->db) ){
		$resp = ($CI->db->limit(1)->get_where($table, array($field => $str))->num_rows() === 1) ? TRUE : FALSE;
	}

	return $resp;
}

function generateToken($salt = '')
{
	$code = strtotime(date('Y-m-d H:i:s'));

	if( is_string($salt) ){
		$code = $code.$salt;
	}

	return md5($code);
}


function pr($array)
{

	print '<pre>';
	print_r($array);
	print '</pre>';

}

function upCase($word = '')
{
	if( $word != '' ){
		$word = mb_convert_case(mb_substr($word,0,1), MB_CASE_UPPER, "UTF-8").mb_substr($word,1,strlen($word));
	}

	return $word;
}
