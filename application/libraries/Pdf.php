<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');



//require_once('../helpers/tcpdf/examples/lang/eng.php');
require_once(APPPATH.'helpers/tcpdf/tcpdf.php');

class Pdf extends TCPDF {


 // footer változó beállítása
  public $footercontent = '';

  public function setFooterContent($page_number) {

    // beállítjuk a publikus változó tartalmát
    $this->footercontent = $page_number;

    //print($this->footercontent);

  }

  
  //Page header
  public function Header() {
    
    $html = 'Some Header';
 
    $this->SetFontSize(8);
    $this->WriteHTML($html, true, 0, true, 0);
  }
 
  // Page footer
  public function Footer() {


    if($this->footercontent == TRUE){

      $html = "<hr><table><tr><td>$this->numpages</td></tr></table>";
      $this->SetY(-15);
      $this->SetFontSize(10);
    }else{

      $html = '' ;
    }

    
 
    
    $this->WriteHTML($html, true, 0, true, 0);
  }

}