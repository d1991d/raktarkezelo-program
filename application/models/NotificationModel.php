<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class NotificationModel extends CI_Model {

	function __construct()
	{
        parent::__construct();
        
        $this->load->model('DatabaseModel');
        $this->load->model('ProductModel');
    }

    function notification() {

        $params['low_amount'] = '1';

        if($this->ProductModel->getProduct($params) != false){

            $low_amount_products = count($this->ProductModel->getProduct($params));

        }else{

            $low_amount_products = 0;

        }


        $db_save_check = $this->DatabaseModel->checkLastdbSave();

        if($low_amount_products > 0){

            $not_num_1 = 1;

            $notification[0] = array(
            '0'     => '/fooldal#low_amount_product_list',
            '1'     => $low_amount_products.' termék raktárkészlete alacsony!',

            );

        }else{
            $not_num_1 = 0;
        }

        if($db_save_check != FALSE){

            $not_num_2 = 1;

            $notification[1] = array(
                '0'     => '/adatbazis',
                '1'     => $db_save_check.' napja nem készítettél adatbázis mentést!'
            );

        }else{

            $not_num_2 = 0;


        }

        if(isset($notification)){

            $notification_data = array(
                'notification_num'  => $not_num_1 + $not_num_2,
                'notifications'     => $notification,
            );

        }else{

            $notification_data = array(
                'notification_num'  => 0,
                'notifications'     => 'Jelenleg nincs értesítésed',
            );
        }

        return  $notification_data;

    }

}