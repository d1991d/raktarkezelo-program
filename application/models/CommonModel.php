<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class CommonModel extends CI_Model {

	function __construct()
	{
		parent::__construct();
    }


    /*function getClientData($string = false){
        //$this->load->library('user_agent');
        if ($this->agent->is_browser())
        {
                $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
                $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
                $agent = $this->agent->mobile();
        }
        else
        {
                $agent = 'Unidentified User Agent';
        }

        if($string){
            return "platform: $agent, OS: {$this->agent->platform()}";
        }

        return [
            "agent" => $agent,
            "platform" => $this->agent->platform(),
        ];
    }*/


    function log($result, $event, $text){


        $data = array(
            'log_user_id'       => $this->session->userdata('user_id'),
            'log_result'        => $result,
            'log_event'         => $event,
            'log_text'          => $text,
            'log_user_agent'    => 'platform: '.$this->agent->browser().' '.$this->agent->version().', OS: '.$this->agent->platform(),
            'log_date'          => date('Y-m-d H:i:s')
        );

        //var_dump($data);exit;

        if( is_array($data) && !empty($data) ){

			$this->db->insert('logs', $data);

			return $this->db->insert_id();
		}
		return FALSE;
        
    }

}