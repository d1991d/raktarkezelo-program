<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MovementModel extends CI_Model {

	function __construct()
	{
		parent::__construct();
    }
    
    
    function saveMovement($movement_data = ''){

        if(is_array($movement_data) && !empty($movement_data)){
			
			$this->db->insert('movements', $movement_data);

			return $this->db->insert_id();
		}
		return FALSE;
	}
	
	function saveMovement_id($movement_data = ''){

        if(is_array($movement_data) && !empty($movement_data)){
			
			$this->db->insert('movement_ids', $movement_data);

			return $this->db->insert_id();
		}
		return FALSE;
    }

    function getMovement($params =''){

        // Lekért mezők
		$this->db->select('*');

		if( isset($params['where']) && $params['where'] != '' ){
			$this->db->where($params['where']);
		}

		// Keresés a megadott kifejezésre
		if( isset($params['keyword']) && $params['keyword'] != '' ){
			$this->db->like('movement_id_code', $params['keyword']);
			//$this->db->or_like('product_item_number', $params['keyword']);
		}

		// Limit beállítása
		if( isset($params['limit']) && is_numeric($params['limit']) ){
			$this->db->limit($params['limit']);
		}


		$query = $this->db->get('Movements');

		//var_dump($this->db->last_query());exit;

		if ( $query->num_rows() > 0 ) {

			$movement = $query->result_array();

			$result = array();

			// Visszatérési tömb összeállítása a DataTable listához
			foreach ($movement as $value) {
				$result[] = array(
                    'movement_id'               	=> $value['movement_id'],
                    'movement_id_code'              => $value['movement_id_code'],
                    'movement_product_id'       	=> $value['movement_product_id'],
                    'movement_old_amount'       	=> $value['movement_old_amount'],
                    'movement_movement_type'   	 	=> $value['movement_movement_type'],
                    'movement_movement_amount'  	=> $value['movement_movement_amount'],
                    'movement_product_amount_unit'  => $value['movement_product_amount_unit'],
                    'movement_new_amount'       	=> $value['movement_new_amount'],
                    'movement_is_deleted'       	=> $value['movement_is_deleted'],
                    'movement_date'             	=> $value['movement_date'],
				);
			}
			
			return $result;
		}
		return false;

	}

	function getStorno($params =''){

        // Lekért mezők
		$this->db->select('*');

		if( isset($params['where']) && $params['where'] != '' ){
			$this->db->where($params['where']);
		}

		// Keresés a megadott kifejezésre
		if( isset($params['keyword']) && $params['keyword'] != '' ){
			$this->db->like('movement_id_code', $params['keyword']);
			//$this->db->or_like('product_item_number', $params['keyword']);
		}

		// Limit beállítása
		if( isset($params['limit']) && is_numeric($params['limit']) ){
			$this->db->limit($params['limit']);
		}


		$query = $this->db->get('storno_movements');

		//var_dump($this->db->last_query());exit;

		if ( $query->num_rows() > 0 ) {

			$movement = $query->result_array();

			$result = array();

			// Visszatérési tömb összeállítása a DataTable listához
			foreach ($movement as $value) {
				$result[] = array(
                    'storno_id'               		=> $value['storno_id'],
                    'storno_id_code'              	=> $value['storno_id_code'],
                    'storno_movement_id_code'       => $value['storno_movement_id_code'],
                    'storno_movement_id'       		=> $value['storno_movement_id'],
                    'storno_date'   	 			=> $value['storno_date'],
				);
			}
			
			return $result;
		}
		return false;

	}

	function getMovementId($params=""){

		$this->db->select('*');
		$this->db->from('movement_ids');

		if( isset($params['where']) && $params['where'] != '' ){
			$this->db->where($params['where']);
		}

		// Keresés a megadott kifejezésre
		if( isset($params['keyword']) && $params['keyword'] != '' ){
			$this->db->like('movement_id_id_code', $params['keyword']);
			//$this->db->or_like('product_item_number', $params['keyword']);
		}
		

		$query = $this->db->get();

		if($query->num_rows() > 0){

			return $query->result_array();

		}else{

			return false;

		}

	}

	function stornoMovement($params = '', $data= ''){

        if( is_array($params) && !empty($params) != '' && is_array($data) && !empty($data) ){

			if( isset($params['where']) && $params['where'] != '' ){
				$this->db->where($params['where']);
			}

			$this->db->update('movements', $data);
			return TRUE;
		}
		return FALSE;
        
    }
	

	function generate_form_id_code( $form_type = ''){

		switch ($form_type) {

			case '0': //bevételezés

				$this->db->select('movement_id_code');
				$this->db->like('movement_id_code', 'BEV');
				$this->db->order_by('movement_id_code', 'DESC');
				$this->db->limit('1');
				$this->db->from('movements');

				$query = $this->db->get()->result_array();

				$type = 'BEV';
			  
				$year = date('Y');			

				if(empty($query)){

					$code = $type.'-'.$year.'-0001';

				}else{

					$last_code = substr($query[0]['movement_id_code'], 9, );

					$code = $last_code + 1;

					$code = $type.'-'.$year.'-'.str_pad($code, 4, '0', STR_PAD_LEFT);

				}

				return $code;

			  break;
			case '1':

				$this->db->select('movement_id_code');
				$this->db->like('movement_id_code', 'KIA');
				$this->db->order_by('movement_id_code', 'DESC');
				$this->db->limit('1');
				$this->db->from('movements');

				$query = $this->db->get()->result_array();

				$type = 'KIA';
			  
				$year = date('Y');			

				if(empty($query)){

					$code = $type.'-'.$year.'-0001';

				}else{

					$last_code = substr($query[0]['movement_id_code'], 9, );

					$code = $last_code + 1;

					$code = $type.'-'.$year.'-'.str_pad($code, 4, '0', STR_PAD_LEFT);

				}

				return $code;

			  break;

			  case '2':

				$this->db->select('storno_id_code');
				$this->db->like('storno_id_code', 'STO');
				$this->db->order_by('storno_id_code', 'DESC');
				$this->db->limit('1');
				$this->db->from('storno_movements');

				$query = $this->db->get()->result_array();

				$type = 'STO';
			  
				$year = date('Y');			

				if(empty($query)){

					$code = $type.'-'.$year.'-0001';

				}else{

					$last_code = substr($query[0]['storno_id_code'], 9, );

					$code = $last_code + 1;

					$code = $type.'-'.$year.'-'.str_pad($code, 4, '0', STR_PAD_LEFT);

				}

				return $code;

				break;
			/*case label3:
			  code to be executed if n=label3;
			  break;*/


		  }

	}

	function saveStorno($storno_data = ''){

        if(is_array($storno_data) && !empty($storno_data)){
			
			$this->db->insert('storno_movements', $storno_data);

			return $this->db->insert_id();
		}
		return FALSE;
	}

}