<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class DatabaseModel extends CI_Model {

	function __construct()
	{
		parent::__construct();
    }

    function saveDB($data = ''){

        if(is_array($data) && !empty($data)){
			
			$this->db->insert('save_db_list', $data);

			return $this->db->insert_id();
		}
		return FALSE;

    }

    function checkLastdbSave() {

        $this->db->from('save_db_list');
        $this->db->order_by('db_id', 'desc');
        $this->db->limit('1');

        $query_array = $this->db->get()->result_array();

        if(is_array($query_array) && !empty($query_array)) {

           $last_save_date= date_create($query_array[0]['db_time_created']);

           //var_dump($last_save_date);exit;

           $now = date_create(date('Y-m-d H:i:s'));

           $diff=date_diff($now,$last_save_date);

           $diff = $diff->format("%d");

           //var_dump($diff); //d - nap, i - perc

           if($diff > $this->config->item('db_save_check_diff_time')) {

               return $diff;

           }else{

               return false;

           }
        }

    }

}