<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* PDF Model
*
* PDF fájl készítése
*
* @package 		Core
* @subpackage   PDF
* @author 		Nagy Dávid
* @copyright 	CodeArt
* @version 		v0.1
*
*/

class PdfModel extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

// ----------------------------------------------------------------------------------------------------------------------------------------

	/**
    * PDF létrehozása
    *
    *   $pdf_name    |string   = PDF neve, amire mentésre kerül
    *   $content     |string   = PDF tartalma, html szövegformázó tag-eket tartalmazva
    *   $save_path   |string   = PDF mentési helye a public mappán belül, ha nem létezik a mappa, akkor az alapértelmezett mentési helyre kerül: public/pdf/
    *   $page_number |boolen    = TRUE vagy FALSE
    *   return                 = PDF megnyitási/letöltési linkje
    */

    function createPdf($pdf_name= '', $content= '', $page_number= '', $save_path='' ) {

        //Megvizsgáljuk, hogy van-e név és tartalom
        if(!empty($pdf_name) && !empty($content))
        {

            //print($page_number);
            //meghívjuk a PDF library-t
            $this->load->library('Pdf');

            //Összeállítjuk a pdf tartalmát
            $obj_pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


            //megvizsgáljuk, hogy kell-e oldalszámozás
            if(!$page_number or $page_number == FALSE){
                print($page_number);
            }else{
                $obj_pdf->setFooterContent($page_number);
                //print($page_number);
            }
            
            $obj_pdf->SetCreator(PDF_CREATOR);                
            $obj_pdf->SetHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
            $obj_pdf->setPrintheader(false);
            $obj_pdf->setPrintfooter(true);
            //$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
            $obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            $obj_pdf->setFont('freesans');
            //$obj_pdf->SetDefaultMonospacedFont('helvetica');
            $obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            $obj_pdf->SetMargins(PDF_MARGIN_LEFT, '20', PDF_MARGIN_RIGHT);
            //$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
            //$obj_pdf->setPrintFooter(false);
            $obj_pdf->SetAutoPageBreak(TRUE, 75);
            $obj_pdf->AddPage();

            ob_start();

            // pdf tartalma
            $content =$content;
            ob_end_clean();

            $obj_pdf->writeHTML($content, true, false, true, false);

            // pdf nevének ékezettelenítése
            $unwanted_array = array(  'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                        'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ő'=>'O', 'Ø'=>'O', 'Ù'=>'U',
                        'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
                        'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
                        'ö'=>'o', 'ő'=> 'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', ' '=>'_' );
            $pdf_name = strtr($pdf_name, $unwanted_array );

            //mentési hely vizsgálata
            if($save_path && !empty($save_path)){
                if(!file_exists(FCPATH.$save_path.'/')){
                    mkdir(FCPATH.$save_path.'/', 0777);
                    $save_path =$save_path.'/';
                }else{
                    $save_path =$save_path.'/';
                }
            }else{
                
                if(!file_exists(FCPATH.'pdf')){
                    mkdir(FCPATH.'pdf', 0777);
                    $save_path = 'pdf/';
                }else{
                    $save_path = 'pdf/';
                }
            }

            
            //pdf mentése
            $obj_pdf->Output(FCPATH.$save_path.$pdf_name.'.pdf', 'F');

            return site_url().$save_path.$pdf_name.'.pdf';
            

        }else{
            return false;
        }

    }

    function getForm($form_id= ''){

        $this->db->from('forms');
        $this->db->where('form_id', $form_id);

        return $this->db->get()->result_array();

    }
}