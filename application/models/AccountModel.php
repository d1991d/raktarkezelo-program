<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class AccountModel extends CI_Model {

	function __construct()
	{
		parent::__construct();
    }
    

	function getUser($params= '') {


		// Lekért mezők
		$this->db->select('*');

		if( isset($params['where']) && $params['where'] != '' ){
			$this->db->where($params['where']);
		}

		$query = $this->db->get('users');

		// Vizsgáljuk van e találat
		if ( $query->num_rows() > 0 ) {

            $user = $query->result_array();
            
            foreach($user as $value) {
                
                $result[] = array(
                    'user_id'				=> $value['user_id'],
                    'user_email'			=> $value['user_email'],
                    'user_username'			=> $value['user_username'],
                    'user_pass'				=> $value['user_pass'],
                    'user_time_created'		=> $value['user_time_created'],
                    'user_last_login'		=> $value['user_last_login'],
                );
            }

            return $result;
		
	    }
    
        return false;
    }

    function editUser($user_id='',$data= ''){

        if( $user_id != '' && is_array($data) && !empty($data) ){

			$this->db->where('user_id', $user_id);
			$this->db->update('users', $data);
			return TRUE;
		}
		return FALSE;
        


    }

    public function checkLoggedIn(){

        if($this->session->has_userdata('user')){
            
            return TRUE;

        }else{
            return FALSE;
        }

    }

    public function checkLogIn(){

        if($this->session->has_userdata('logged_in') && $this->session->userdata('logged_in') == 'YES'){
            
            return TRUE;

        }else{
            return FALSE;
        }

    }


    public function gotoLockscreen() {

        $now = date_create(date('H:i:s'));

        $diff = date_diff($now,$this->session->userdata('click_time')); 
        

        //var_Dump($now, $this->session->userdata('click_time'), $diff = date_diff($now,$this->session->userdata('click_time')), $diff->format("%i minutes"));
        if($diff->i > 15){


            $this->session->unset_userdata('user');
            $this->session->unset_userdata('click_time');

            redirect('/jelentkezz_be');

        }else{

            
            
        }

    }
}