<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class AttributesModel extends CI_Model {

	function __construct()
	{
		parent::__construct();
    }
   

    function getAttribute($params= ''){

        // Lekért mezők
		$this->db->select('*');

		if( isset($params['where']) && $params['where'] != '' ){
			$this->db->where($params['where']);
		}

		
		// Keresés a megadott kifejezésre
		if( isset($params['keyword']) && $params['keyword'] != '' ){
			$this->db->like('attribute_shortname', $params['keyword']);
			$this->db->or_like('attribute_logname', $params['keyword']);
		}

		// Rendezés a megadott oszlop szerint
		if( isset($params['order']) && $params['order'] != '' && isset($params['sort']) && $params['sort'] != '' ){
			$this->db->order_by($params['order'], $params['sort']);
		}

		// Limit beállítása
		if( isset($params['limit']) && is_numeric($params['limit']) ){
			$this->db->limit($params['limit']);
		}

		// Lekérési mennyiség beállítása a lapozáshoz
		if( isset($params['start']) && is_numeric($params['start']) && isset($params['length']) && is_numeric($params['length']) ){
			$this->db->limit($params['length'], $params['start']);
		}

		$query = $this->db->get('attributes');
		
		//var_dump($this->db->last_query());exit;

        // Vizsgáljuk van e találat
		if ( $query->num_rows() > 0 ) {

			$product = $query->result_array();

			// Ha van where feltétel akkor a sima tömböt adjuk vissza, ha nincs akkor a formázottat a listához
			if( isset($params['where']) && $params['where'] != '' ){

				// Visszatérési tömb összeállítása a DataTable listához
				foreach ($product as $value) {
					$result[] = array(
						'attribute_id'				=> $value['attribute_id'],
						'attribute_select'          => '<input type="checkbox" class="check" name="attributeselect['.$value['attribute_id'].']" value="'.$value['attribute_id'].'">',
						'attribute_shortname'       => $value['attribute_shortname'],
						'attribute_longname'        => $value['attribute_longname'],
						'attribute_time_created'	=> $value['attribute_time_created'],
						'attribute_last_edit'	    => $value['attribute_last_edit'],
						'attribute_check'	        => '<a class="btn btn-primary" href="/tulajdonsagok/'.$value['attribute_id'].'">Megtekint</a>',
					);
				}
			}
			else{
				$result = array();

				// Visszatérési tömb összeállítása a DataTable listához
				foreach ($product as $value) {
					$result[] = array(
						'attribute_id'				=> $value['attribute_id'],
						'attribute_select'          => '<input type="checkbox" class="check" name="attributeselect['.$value['attribute_id'].']" value="'.$value['attribute_id'].'">',
						'attribute_shortname'       => $value['attribute_shortname'],
						'attribute_longname'        => $value['attribute_longname'],
						'attribute_time_created'	=> $value['attribute_time_created'],
						'attribute_last_edit'	    => $value['attribute_last_edit'],
						'attribute_check'	        => '<a class="btn btn-primary" href="/tulajdonsag/'.$value['attribute_id'].'">Megtekint</a>',
					);
				}
			}
			return $result;
		}

		return FALSE;
    }

    function getAttributeNum($params = '')
	{
		$this->db->select('*');

		if(isset($params['keyword']) && $params['keyword'] != ''){
			$this->db->like('attribute_shortname', $params['keyword']);
			$this->db->or_like('attribute_logname', $params['keyword']);
		}

		return $this->db->count_all_results('attributes');
    }
    
    function deleteAttribute($select_id = '') {

        if(is_array($select_id) && !empty($select_id)){

            foreach($select_id as $key => $value){
                if( is_numeric($value) && $value != 0 ){
                    $this->db->where('attribute_id', $value);
                    $this->db->delete('attributes');
                }
            }
            return TRUE;
        }else{
            return FALSE;
        }

	}
	
	function editAttribute($attr_id='',$data= ''){

        if( $attr_id != '' && is_array($data) && !empty($data) ){

			$this->db->where('attribute_id', $attr_id);
			$this->db->update('attributes', $data);
			return TRUE;
		}
		return FALSE;
        
	}
	
	function addAttribute($data = ''){

		if(is_array($data) && !empty($data)){
			
			$this->db->insert('attributes', $data);

			return $this->db->insert_id();
		}
		return FALSE;
	}
    
}