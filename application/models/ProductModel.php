<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class ProductModel extends CI_Model {

	function __construct()
	{
		parent::__construct();
    }
   

    function getProduct($params= ''){

        // Lekért mezők
		$this->db->select('*');

		if( isset($params['where']) && $params['where'] != '' ){
			$this->db->where($params['where']);
		}

		
		// Keresés a megadott kifejezésre
		if( isset($params['keyword']) && $params['keyword'] != '' ){
			$this->db->like('product_name', $params['keyword']);
			$this->db->or_like('product_item_number', $params['keyword']);
		}

		// Rendezés a megadott oszlop szerint
		if( isset($params['order']) && $params['order'] != '' && isset($params['sort']) && $params['sort'] != '' ){
			$this->db->order_by($params['order'], $params['sort']);
		}

		// Limit beállítása
		if( isset($params['limit']) && is_numeric($params['limit']) ){
			$this->db->limit($params['limit']);
		}

		if(isset($params['inactive']) && $params['inactive'] == '1'){
			$this->db->where('product_status', '0');
		}else{
			$this->db->where('product_status', '1');
		}

		if( isset($params['low_amount']) ){
			$this->db->where('product_status', 1)->where('(product_min_amount + '.$this->config->item('min_amount_plus').') >= product_amount');
		}

		// Lekérési mennyiség beállítása a lapozáshoz
		if( isset($params['start']) && is_numeric($params['start']) && isset($params['length']) && is_numeric($params['length']) ){
			$this->db->limit($params['length'], $params['start']);
		}

		$query = $this->db->get('products');
		
		//var_dump($this->db->last_query());exit;

        // Vizsgáljuk van e találat
		if ( $query->num_rows() > 0 ) {

			$product = $query->result_array();

			// Ha van where feltétel akkor a sima tömböt adjuk vissza, ha nincs akkor a formázottat a listához
			if( isset($params['where']) && $params['where'] != '' ){

				// Visszatérési tömb összeállítása a DataTable listához
				foreach ($product as $value) {
					$result[] = array(
						'product_id'			=> $value['product_id'],
						'product_select'        => '<input type="checkbox" class="check" name="productselect['.$value['product_id'].']" value="'.$value['product_id'].'">',
						'product_select_radio'  => '<input type="radio" class="check" name="productselect" value="'.$value['product_id'].'">',
						'product_name'          => $value['product_name'],
						'product_item_number'   => $value['product_item_number'],
						'product_amount'	    => $value['product_amount'],
						'product_amount_unit'	=> $value['product_amount_unit'],
						'product_min_amount'	=> $value['product_min_amount'],
						'product_status'	    => ($value['product_status'] == '0')? 'Kikapcsolva' : 'Bekapcsolva',
						'product_time_created'	=> $value['product_time_created'],
						'product_last_edit'	    => $value['product_last_edit'],
						'product_check'	        => '<a class="btn btn-primary" href="/termek/'.$value['product_id'].'">Megtekint</a>',
					);
				}
			}
			else{
				$result = array();

				// Visszatérési tömb összeállítása a DataTable listához
				foreach ($product as $value) {
					$result[] = array(
						'product_id'			=> $value['product_id'],
						'product_select'        => '<input type="checkbox" class="check" name="productselect['.$value['product_id'].']" value="'.$value['product_id'].'">',
						'product_select_radio'  => '<input type="radio" class="check" name="productselect" value="'.$value['product_id'].'">',
                        'product_name'          => $value['product_name'],
                        'product_item_number'   => $value['product_item_number'],
                        'product_amount'	    => $value['product_amount'],
                        'product_amount_unit'	=> $value['product_amount_unit'],
                        'product_min_amount'	=> $value['product_min_amount'],
						'product_status'	    => ($value['product_status'] == '0')? 'Kikapcsolva' : 'Bekapcsolva',
						'product_time_created'	=> $value['product_time_created'],
						'product_last_edit'	    => $value['product_last_edit'],
						'product_check'	        => '<a class="btn btn-primary" href="/termek/'.$value['product_id'].'">Megtekint</a>',
					);
				}
			}
			return $result;
		}

		return FALSE;
	}
	
	function getProducts($params =''){
		
		
		// Lekért mezők
		$this->db->select('*');

		if( isset($params['where']) && $params['where'] != '' ){
			$this->db->where($params['where']);
		}

		$query = $this->db->get('Products');

		if ( $query->num_rows() > 0 ) {

			$product = $query->result_array();


			$result = array();

			// Visszatérési tömb összeállítása a DataTable listához
			foreach ($product as $value) {
				$result[] = array(
					'product_id'        => $value['product_id'],
					'product_name'          => $value['product_name'],
					'product_item_number'   => $value['product_item_number'],
					'product_amount'	    => $value['product_amount'],
					'product_amount_unit'	=> $value['product_amount_unit'],
					'product_min_amount'	=> $value['product_min_amount'],
					'product_status'	    => ($value['product_status'] == '0')? 'Kikapcsolva' : 'Bekapcsolva',
					'product_time_created'	=> $value['product_time_created'],
					'product_last_edit'	    => $value['product_last_edit'],
					'product_check'	        => '<a class="btn btn-primary" href="/termek/'.$value['product_id'].'">Megtekint</a>',
				);
			}
			
			return $result;
		}
		return false;

	}

    function getProductNum($params = '')
	{
		$this->db->select('*');

		if(isset($params['keyword']) && $params['keyword'] != ''){
			$this->db->like('product_name', $params['keyword']);
			$this->db->or_like('product_item_number', $params['keyword']);
		}
		if( isset($params['low_amount']) ){
			$this->db->where('product_status', 1)->where('(product_min_amount + '.$this->config->item('min_amount_plus').') >= product_amount');
		}

		if( isset($params['inactive']) ){
			$this->db->where('product_status', 0);
		}

		return $this->db->count_all_results('products');
	}

    function editProduct($product_id='',$data= ''){

        if( $product_id != '' && is_array($data) && !empty($data) ){

			$this->db->where('product_id', $product_id);
			$this->db->update('products', $data);
			return TRUE;
		}
		return FALSE;
        
    }

    function deleteProduct($select_id = '') {

        if(is_array($select_id) && !empty($select_id)){

            foreach($select_id as $key => $value){
                if( is_numeric($value) && $value != 0 ){
                    $this->db->where('product_id', $value);
                    $this->db->delete('products');
                }
            }
            return TRUE;
        }else{
            return FALSE;
        }

	}
	
	function productIdandproductItemNumCheck($product_id = '', $product_item_number = ''){

		if($product_id != '' && is_numeric($product_id) && $product_item_number != '' && is_numeric($product_item_number)){

			$params = array('where' => array('product_id' => $product_id));

			$product = $this->getProduct($params);

			if($product[0]['product_item_number'] == $product_item_number){

				return true;

			}else{

				return false;

			}

			//var_dump($product[0]);
		}

	}


	function addProduct($data = ''){

		if(is_array($data) && !empty($data)){
			
			$this->db->insert('products', $data);

			return $this->db->insert_id();
		}
		return FALSE;
	}

}