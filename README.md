# Raktárkészlet kezelő program */ Fejlesztés alatt /*

Raktárkészlet kezelő program alapvető raktárkezelési műveletekhez mint például:

- termék felvétel, szerkesztés
- termék tulajdonság felvétel, szerkesztés
- bevételezés, bizonylat készítésse
- kiadás, bizonylat készítésse
- bevételezés/kiadás sztornózása bizonylat készítésse (fejlesztés alatt)
- listák lekérdezése .pdf-ben pl: bevételezési -, kiadási -, sztornó bizonylat, termék mozgás
	adott időszakra stb (fejlesztés alatt)
- adatbázis biztonsági mentés
- alacsony raktárkészlet jelzése beállított mennyiségnél
- hiánykészlet jelzése
- Lockscreen bizonyos inaktivitás után

# Felhasznált "eszközkészlet":

- HTML
- CSS
- Bootstrap
- Jquery
- Ajax
- Codeigniter 3.
- Sweetalert
- iziToast
- AdminLTE 2.4.3
